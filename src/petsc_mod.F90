
! #define DEBUG

!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!> @file petsc_mod.F90
!> @brief Fortran module defining interface to subprograms on petsc library
!> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
!> @copyright &copy; 2024- Shigeru Tsukamoto. All rights reserved.
!--
module petsc_mod
  implicit none
  private

  !-- scope & interface of petsc_get_version
  public :: petsc_get_version
  interface petsc_get_version
    module function petsc_get_version() result( version )
      implicit none
      character(len=:), allocatable :: version !< version string
    end function petsc_get_version
  end interface petsc_get_version

end module petsc_mod
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
