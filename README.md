# ZENDEN (All-Electron Electronic-Structure Simulation of an Atom)

<!--
image or gif
--->

## Overview
ZENDEN is a short form of the japanese words *ZENDENSHI-KEISAN*, which means all-electron electronic-structure calculation. This code calculates the electronic structure of an atom based on the density functional theory (DFT). Assuming that the system has spherical symmetry, only the radial parts of the wave functions for respective electronic states are calculated, because the wave function is know to be separated into the radial and angular parts.

The code solves the Kohn-Sham equation with/without scalar-relativistic effect for the radial wave functions using a eigensolver. Then, charge-density distribution on radial grids is produced, and the code calculates an effective potential for the Kohn-Sham equation, which is to be solved for revised radial wave functions. This procedure is repeated until the total energy of the system converges.

One can choose equi-distant or logarithmic radial grids. For the exchange-correlation interaction, X-$\alpha$ method, LDA (PZ81, VWN), and GGA are available. For updating charge-density distribution, one can choose the straight-mixing scheme or the Broyden's second method.

## Features
To get accurate result, such as eigenenergies and total energy,
- scalar-relativistic effect can be included by switching it on in the input file, as well as conventional non-relativistic calculations.
- radial grid is discretized based on the logarithmic scale, so that the grid is denser in the vicinity to the nuclei and courser far away from it.
- radial wave functions are interpolated by means of the Lagrange interpolation to suppress unphysical behavior in GGA exchange-correlation potential.
- electrostatic potential is calculated by numerically accumulating the primitive function, which is approximately found from the Lagrange interpolation of the integrand function.
- integration in calculating total energy also employs the Lagrange interpolation of the integrand function.

In input file, one can choose the following options for each of the keywords below:
- Scalar-Relativistic Effect `ScalarRelativistic`[^9][^10]
- Eigensolver `SolverMode`
  - [x] SLEPc `slepc`
  - [x] LAPACK `lapack`
  - [x] conjugate gradient (CG) method[^5][^6] `cg`
  - [x] Davidson method `davidson`
  - [ ] RMM-DIIS (in preparation) `diis`
- Exchange-Correlation Interaction `ExchangeCorrelationType`
  - [x] X-$\alpha$ method `xalpha` (only exchange interaction)
  - [x] LDA-PZ81[^7] `pz81`
  - [x] LDA-VWN[^1] `vwn`
  - [ ] GGA-PW91 `pw91`
  - [x] GGA-PBE[^8] `pbe`
- Charge-Density Mixing `MixingScheme`
  - [x] straight mixing scheme `simple`
  - [x] Broyden's second method[^2][^3][^4] `broyden2nd`

[^1]: [S. H. Vosko, L. Wilk, and M. Nusair, *Can. J. Phys.* **58**, 1200 (1980).](https://doi.org/10.1139/p80-159)
[^2]: [G. P. Srivastava, *J. Phys. A: Math. Gen.* **17**, L317 (1984).](https://doi.org/10.1088/0305-4470/17/6/002)
[^3]: [D. Singh, H. Krakauer, and C. S. Wang, *Phys. Rev. B* **34**, 8391 (1986).](https://doi.org/10.1103/PhysRevB.34.8391)
[^4]: [D. D. Johnson, *Phys. Rev. B* **38**, 12807 (1988).](https://doi.org/10.1103/PhysRevB.38.12807)
[^5]: [D. M. Bylander, L. Kleinman, and S. Lee, *Phys. Rev. B* **42**, 1394 (1990).](https://doi.org/10.1103/PhysRevB.42.1394)
[^6]: [C. K. Gan, P. D. Haynes, and M. C. Payne, *Comput. Phys. Commun.* **134**, 33 (2001).](https://doi.org/10.1016/S0010-4655(00)00188-0)
[^7]: [J. P. Perdew and A. Zunger, *Phys. Rev. B* **23**, 5048 (1981).](https://doi.org/10.1103/PhysRevB.23.5048)
[^8]: [J. P. Perdew, K. Burke, and M. Ernzerhof, Phys. Rev. Lett., **77**, 3865 (1996).](https://doi.org/10.1103/PhysRevLett.77.3865)
[^9]: [D. D. Koelling and B. N. Harmon, *J. Phys. C: Solid State Phys.* **10**, 3107 (1977)](https://doi.org/10.1088/0022-3719/10/16/019)
[^10]: [B. A. Shadwick, J. D. Talman, and M. R. Norman, *Comput. Phys. Commun* **54**, 95 (1989).](https://doi.org/10.1016/0010-4655(89)90035-0)

## Requirement
- Fortran compiler, e.g., gcc (https://gcc.gnu.org/)
- MPI environment, e.g., Open MPI (https://www.open-mpi.org/)
- cmake (https://cmake.org/)
- make
- JSON-Fortran (https://jacobwilliams.github.io/json-fortran/)
- SLEPc (https://slepc.upv.es/)
- PETSc (https://petsc.org/)
- LAPACK (https://www.netlib.org/lapack/)
- BLAS (https://www.netlib.org/blas/) or OpenBLAS (https://www.openblas.net/)

## Usage
It is preferable to make a working directory `build` just in the repository root, and to make, compile, and executing the code. The input file `input.json` can be copied from the repository root, and you can edit it as you want. The executive is named `zenden`, and the name of the input file follows it when you run the code.
```
cd [REPOSITORY_ROOT]
mkdir build
cd build
cmake ..
make
cp ../input.json ./
./zenden ./input.json
```

The project contains a supporting python script `display.py`, which visualizes the radial wave functions and charge-density distribution on radial grids. Assuming that the simulation was performed at the directory `[REPOSITORY_ROOT]/build`, the visualization can be done as follows:
```
cd [REPOSITORY_ROOT]/build
python ../display.py
```

## Input File
Input file is written in a JSON form. An example for a Si atom is shown below:
```json
{
  !-- grid parameters
  "Grid":{
    !-- grid type uniform/logarithm
    "Type":"logarithm",
    !-- number of grids
    "N":1001,
    !-- minimum radius
    "RMin":1.0e-6,
    !-- maximum radius
    "RMax":60.0,
  },
  !-- finite-difference order
  "FiniteDifferenceOrder":4,
  !-- atomic number
  "AtomicNumber":6,
  !-- State
  "State":{
    "1s":2,
    "2s":2,
    "2p":2,
  },
  !-- self-consistent method
  "SelfConsistent":{
    !-- self-consistent loop maximum
    "IterationMax":100,
    !-- energy convergence
    "Tolerance":1.0e-6,
    !-- mixing scheme simple/broyden2nd
    "MixingScheme":"broyden2nd",
    !-- mixing parameter
    "MixingRate":0.5,
    !-- Coulomb interaction
    "CoulombInteraction":true,
  },
  !-- mode of eigensolver slepc/lapack/cg/davidson/diis
  "SolverMode":"davidson",
  !-- exchange-correlation xalpha/pz81/vwn/pbe
  "ExchangeCorrelationType":"pbe",
  !-- scalar-relativistic effect true/false
  "ScalarRelativistic":"true",
}
```

## Selected Results
The eigenenergies calculated by zenden with and without scalar-relativistic effect are compared with the pseudopotential database of GPAW,[^21] ATOMPAW,[^22] and NIST.[^23] The calculations were performed using GGA-PBE.
[^21]:[GPAW, Atomic PAW Setups](https://gpaw.readthedocs.io/setups/setups.html#periodic-table)
[^22]:[ATOMPAW datasets](http://users.wfu.edu/natalie/papers/pwpaw/PAWDatasets.html)
[^23]:[NIST, Atomic Reference Data for Electronic Structure Calculations](http://physics.nist.gov/PhysRefData/DFTdata/contents.html)

##### 1. H (Hydrogen)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -6.493 eV | -6.493 eV | -6.494 eV | -6.332 eV | -6.353 eV |

##### 5. B (Boron)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -180.533 eV | -180.589 eV | -180.588 eV | -179.497 eV | -178.590 eV |
| 2s   |   -9.439 eV |   -9.443 eV |   -9.443 eV |   -9.356 eV |   -9.382 eV |
| 2p   |   -3.609 eV |   -3.607 eV |   -3.606 eV |   -3.557 eV |   -3.716 eV |

##### 6. C (Carbon)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -273.250 eV | -273.379 eV | -273.374 eV | -271.924 eV | -270.649 eV |
| 2s   |  -13.739 eV |  -13.751 eV |  -13.751 eV |  -13.637 eV |  -13.635 eV |
| 2p   |   -5.289 eV |   -5.286 eV |   -5.285 eV |   -5.237 eV |   -5.418 eV |

##### 7. N (Nitrogen)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -384.463 eV | -384.719 eV | -384.706 eV | -382.847 eV | -381.240 eV |
| 2s   |  -18.557 eV |  -18.582 eV |  -18.583 eV |  -18.431 eV |  -18.414 eV |
| 2p   |   -7.095 eV |   -7.090 eV |   -7.089 eV |   -7.041 eV |   -7.243 eV |

##### 14. Si (Silicon)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -1781.067 eV | -1785.944 eV | -1785.696 eV | -1777.605 eV | -1775.882 eV |
| 2s   |  -138.836 eV |  -139.500 eV |  -139.488 eV |  -138.474 eV |  -138.608 eV |
| 2p   |   -95.593 eV |   -95.561 eV |   -95.559 eV |   -95.457 eV |   -95.589 eV |
| 3s   |   -10.768 eV |   -10.813 eV |   -10.812 eV |   -10.724 eV |   -10.872 eV |
| 3p   |    -4.091 eV |    -4.081 eV |    -4.081 eV |    -4.066 eV |    -4.164 eV |

##### 20. Ca (Calcium)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -3927.915 eV | -3949.429 eV | -3948.629 eV | -3922.652 eV | -3929.932 eV |
| 2s   |  -410.665 eV |  -414.371 eV |  -414.318 eV |  -410.071 eV |  -412.548 eV |
| 2p   |  -334.457 eV |  -334.747 eV |  -334.744 eV |  -334.189 eV |  -334.379 eV |
| 3s   |   -46.686 eV |   -47.090 eV |   -47.086 eV |   -46.553 eV |   -46.788 eV |
| 3p   |   -28.059 eV |   -28.010 eV |   -28.008 eV |   -28.001 eV |   -27.992 eV |
| 3d   |    -2.069 eV |    -1.949 eV |  &#x2E3A; eV |    -2.079 eV |  &#x2E3A; eV |
| 4s   |    -3.753 eV |    -3.766 eV |    -3.766 eV |    -3.751 eV |    -3.861 eV |
| 4p   |    -1.419 eV |    -1.403 eV |  &#x2E3A; eV |    -1.403 eV |  &#x2E3A; eV |

##### 27. Co (Cobalt)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -7514.983 eV | -7593.130 eV | -7590.091 eV | -7508.548 eV | -7556.073 eV |
| 2s   |  -882.854 eV |  -898.994 eV |  -898.765 eV |  -882.043 eV |  -895.296 eV |
| 2p   |  -766.565 eV |  -769.950 eV |  -769.968 eV |  -766.069 eV |  -768.585 eV |
| 3s   |   -99.881 eV |  -102.395 eV |  -102.374 eV |   -99.629 eV |  -101.654 eV |
| 3p   |   -65.104 eV |   -65.635 eV |   -65.642 eV |   -64.978 eV |   -65.452 eV |
| 3d   |    -8.511 eV |    -8.207 eV |    -8.204 eV |    -8.536 eV |    -8.487 eV |
| 4s   |    -5.365 eV |    -5.469 eV |    -5.469 eV |    -5.384 eV |    -5.664 eV |
| 4p   |    -1.307 eV |    -1.277 eV |    -1.277 eV |  &#x2E3A; eV |  &#x2E3A; eV |

##### 47. Ag (Silver)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -24527.902 eV | -25330.545 eV | -25304.723 eV | -25316.318 eV | -25188.890 eV |
| 2s   |  -3536.837 eV |  -3729.501 eV |  -3726.880 eV |  -3727.703 eV |  -3713.713 eV |
| 2p   |  -3291.241 eV |  -3345.250 eV |  -3345.415 eV |  -3344.249 eV |  -3336.891 eV |
| 3s   |   -645.295 eV |   -683.676 eV |   -683.243 eV |   -683.138 eV |   -680.580 eV |
| 3p   |   -546.317 eV |   -557.491 eV |   -557.543 eV |   -557.216 eV |   -556.254 eV |
| 3d   |   -363.377 eV |   -358.507 eV |   -358.497 eV |   -358.493 eV |   -358.777 eV |
| 4s   |    -87.740 eV |    -95.030 eV |    -94.965 eV |    -94.969 eV |    -94.755 eV |
| 4p   |    -56.691 eV |    -58.560 eV |    -58.577 eV |    -58.514 eV |    -58.559 eV |
| 4d   |     -7.909 eV |     -7.447 eV |     -7.446 eV |     -7.453 eV |     -7.683 eV |
| 5s   |     -3.991 eV |     -4.403 eV |     -4.401 eV |     -4.421 eV |     -4.694 eV |
| 5p   |     -0.765 eV |     -0.737 eV |     -0.738 eV |     -0.669 eV |   &#x2E3A; eV |

##### 79. Au (Gold)

|      | ZENDEN<br>non-rel. | ZENDEN<br>sca.-rel. | GPAW | ATOMPAW | NIST<br>ScRLDA |
| :--: | -----: | -----: | ---: | ------: | ---: |
| 1s   | -73070.592 eV | -80788.861 eV | -80481.161 eV | &#x2E3A; eV | -80221.184 eV |
| 2s   | -12193.927 eV | -14253.835 eV | -14210.116 eV | &#x2E3A; eV | -14169.478 eV |
| 2p   | -11723.403 eV | -12320.545 eV | -12322.340 eV | &#x2E3A; eV | -12269.305 eV |
| 3s   |  -2853.742 eV |  -3364.088 eV |  -3354.481 eV | &#x2E3A; eV |  -3346.059 eV |
| 3p   |  -2631.881 eV |  -2803.551 eV |  -2804.156 eV | &#x2E3A; eV |  -2793.814 eV |
| 3d   |  -2217.869 eV |  -2205.399 eV |  -2205.651 eV | &#x2E3A; eV |  -2201.887 eV |
| 4s   |   -601.817 eV |   -730.091 eV |   -727.763 eV | &#x2E3A; eV |   -725.473 eV |
| 4p   |   -506.096 eV |   -546.355 eV |   -546.571 eV | &#x2E3A; eV |   -543.990 eV |
| 4d   |   -330.117 eV |   -325.920 eV |   -325.987 eV | &#x2E3A; eV |   -325.511 eV |
| 4f   |    -94.402 eV |    -81.173 eV |    -81.172 eV | &#x2E3A; eV |    -81.936 eV |
| 5s   |    -84.492 eV |   -108.328 eV |   -107.890 eV | &#x2E3A; eV |   -108.202 eV |
| 5p   |    -54.254 eV |    -59.767 eV |    -59.818 eV | &#x2E3A; eV |    -59.862 eV |
| 5d   |     -8.069 eV |     -6.881 eV |     -6.891 eV | &#x2E3A; eV |     -7.141 eV |
| 6s   |     -4.120 eV |     -5.758 eV |     -5.728 eV | &#x2E3A; eV |     -6.041 eV |
| 6p   |     -0.825 eV |     -0.770 eV |     -0.775 eV | &#x2E3A; eV |   &#x2E3A; eV |

## Author
Shigeru Tsukamoto

E-Mail: tsukamoto.shigeru@gmail.com

<!--
[twitter](https://twitter.com/Kotabrog)
--->

<!---
## Licence
```{include} ./LICENSE
```

[MIT](https://......)
--->
