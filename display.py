#!/usr/bin/env python
# -*- coding: utf-8 -*-

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
def load_state( filename:str ) -> dict:

  #-- load data
  with open( filename, 'r' ) as fp:
    lines = [ line.strip() for line in fp.readlines() ]

  #-- check number of states
  nstate = len( lines[0].split( ':' )[1].strip().split() )

  #-- analyze data
  data = [ { 'values':[] } for i in range( nstate ) ]
  for line in lines:
    if line.startswith( '#' ):
      key, buf = line.split( '#' )[1].strip().split( ':' )
      if key == 'psi': continue
      for i, val in enumerate( buf.strip().split() ):
        data[i][key] = val
    else:
      for i, val in enumerate( line.strip().split() ):
        data[i]['values'].append( float( val ) )

  #-- sort data
  buf = sorted( { float( datum['eigenenergy'] ):i for i, datum in enumerate( data ) }.items() )
  buf = [ item[1] for item in buf ]

  return [ data[i] for i in buf ]

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
def parse_arguments():

  #-- import modules
  import argparse

  #-- construct argparse instance
  parser = argparse.ArgumentParser( prog='zenden vis', description='visualize zenden output data.' )

  #-- set positional arguments
  parser.add_argument( 'files', type=ascii, nargs='+',
                       help='names of files to be read followed by keywords. filename:key1,key2' )

  #-- set optional arguments
  parser.add_argument( '--debug', dest='debug', action='store_true', \
                       help='debug mode' )

  #-- set optional arguments
  parser.add_argument( '--xrange', dest='xrange', type=float, nargs=3, default=[ 0, 10, 11 ], \
                       help='range of x coordinate. xmin,xmax,numx' )

  #-- set optional arguments
  parser.add_argument( '--yrange', dest='yrange', type=float, nargs=3, default=[ -0.5, 0.5, 11 ], \
                       help='range of y coordinate. ymin,ymax,numy' )

  return parser.parse_args()

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
def parse_files( files ) -> dict:
  d = {}
  for file in [ item.strip( "'" ) for item in files ]:
    if file.find( ':' ) == 0:
      raise Exception( 'ERROR: colon ":" found at invalid position. arg: %s' % file )
    elif file.find( ':' ) == -1 or file.find( ':' ) == len( file ) - 1:
      d[file] = ''
    else:
      buf1, buf2 = file.split( sep=':', maxsplit=1 )
      d[buf1] = buf2.split(',')
  return d

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
def display_data( targets, xlim:list=[ 0.0, 1.0, 11 ], nxticks:int=11, ylim:list=[ -0.5, 0.5, 11 ], \
                  nyticks:int=11, markersize:int=1, linewidth:float=0.5, debug=False ) -> int:

  #-- import modules
  import pandas as pd
  import matplotlib.pyplot as plt
  import numpy as np

  #-- load data
  d = {}
  for filename, keys in targets.items():
    buf = pd.read_csv( filename )
    if len( keys ) == 0:
      d[filename] = buf
    else:
      icols = [ 0 ]
      for i, column in enumerate( buf.columns ):
        if i == 0: continue
        if column.strip().split( ":" )[1] in keys: icols.append( i )
      d[filename] = buf.iloc[:,icols]
    if debug:
      print( filename, d[filename].columns )

  #-- set module parameters
  plt.rcParams["font.size"] = 16
  # plt.rcParams['text.usetex'] = True
  figsize = ( 16, 8 )
  xlabel = r'radial grid $r$ (a.u.)'
  ylabel = r'values'

  #-- create instance
  fig, ax = plt.subplots( nrows=1, ncols=1, figsize=figsize )

  #-- plot data
  for key, df in d.items():
    if debug: print( key, df.columns)
    x = df.iloc[:,0]
    for column in df.columns[1:]:
      y = df[column]
      if debug: print( min( x ), max( x ), min( y ), max( y ) )
      label = df[column].name.strip().split( ":" )[1]
      ax.plot( x, y, marker='o', markersize=markersize, linewidth=linewidth, label=label )

  #-- set outlook
  ax.set_xlabel( xlabel )
  ax.set_ylabel( ylabel )
  ax.set_xlim( xlim )
  ax.set_ylim( ylim )
  ticks = np.linspace( xlim[0], xlim[1], nxticks )
  ax.set_xticks( ticks )
  ticks = np.linspace( ylim[0], ylim[1], nyticks )
  ax.set_yticks( ticks )
  ax.tick_params( axis='both', which='both', direction='in' )
  ax.legend()
  ax.grid()

  #-- display plot
  fig.tight_layout()
  plt.show()

  return 0

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
if __name__ == "__main__":
  import sys

  try:
    args = parse_arguments()
    targets = parse_files( args.files )
    xlim    = [ float( item ) for item in args.xrange[0:2] ]
    nxticks = int( args.xrange[2] )
    ylim    = [ float( item ) for item in args.yrange[0:2] ]
    nyticks = int( args.yrange[2] )
    ret = display_data( targets=targets, xlim=xlim, nxticks=nxticks, ylim=ylim, nyticks=nyticks, \
                        debug=args.debug )
  except Exception as errmsg:
    print( 'ERROR: %s' % errmsg )

  sys.exit( 0 )
