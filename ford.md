Title:   ZENDEN
Summary: all-electron calculation of a spherical-symmetric atom
Author:  Shigeru Tsukamoto
Email:   tsukamoto.shigeru@gmail.com
Blank-value:
Project: zenden
License: by-nc-nd
Project-gitlab: https://gitlab.com/tsuka1/zenden
Blank-value:
Src_dir: ./src
Output_dir: ./ford
Graph: true
Fixed_length_limit: False

