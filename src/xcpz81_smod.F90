
! #define DEBUG

!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!> @file xcpz81_smod.F90
!> @brief Fortran submodule providing implementation of exchange-correlation interaction PZ81.
!> @details The exchage-correlation energy @& potential are based on the local (spin) density
!!          approximation, and are calculated using the formulae proposed by Perdew @& Zunger.
!!          @cite PRB23-005048
!> @note The exchange-correlation energy/potential obtained from the subprogram in this module are
!!       in Hartree atomic unit.
!> @note The exchange-correlation energy/potential in Ref. @cite PRB23-005048 are written also in the
!!       Hartree atomic unit.
!> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
!> @copyright &copy; 2021- Shigeru Tsukamoto. All rights reserved.
!--
submodule(xcpz81_mod) xcpz81_smod
  implicit none

  !-- The following parameters are given in TABLE VII and Appendix C of Ref. @cite PRB23-005048.
  real(8), parameter :: GAMMA_U = -0.1423d0   !< \f$\gamma^{\mathrm{U}}\f$ for non-spin-polarized
  real(8), parameter :: BETA1_U =  1.0529d0   !< \f$\beta_{1}^{\mathrm{U}}\f$ for non-spin-polarized
  real(8), parameter :: BETA2_U =  0.3334d0   !< \f$\beta_{2}^{\mathrm{U}}\f$ for non-spin-polarized
  real(8), parameter :: A_U     =  0.0311d0   !< \f$A^{\mathrm{U}}\f$ for non-spin-polarized
  real(8), parameter :: B_U     = -0.048d0    !< \f$B^{\mathrm{U}}\f$ for non-spin-polarized
  real(8), parameter :: C_U     =  0.0020d0   !< \f$C^{\mathrm{U}}\f$ for non-spin-polarized
  real(8), parameter :: D_U     = -0.0116d0   !< \f$D^{\mathrm{U}}\f$ for non-spin-polarized
  real(8), parameter :: GAMMA_P = -0.0843d0   !< \f$\gamma^{\mathrm{P}}\f$ for spin-polarized
  real(8), parameter :: BETA1_P =  1.3981d0   !< \f$\beta_{1}^{\mathrm{P}}\f$ for spin-polarized
  real(8), parameter :: BETA2_P =  0.2611d0   !< \f$\beta_{2}^{\mathrm{P}}\f$ for spin-polarized
  real(8), parameter :: A_P     =  0.01555d0  !< \f$A^{\mathrm{P}}\f$ for spin-polarized
  real(8), parameter :: B_P     = -0.0269d0   !< \f$B^{\mathrm{P}}\f$ for spin-polarized
  real(8), parameter :: C_P     =  0.0007d0   !< \f$C^{\mathrm{P}}\f$ for spin-polarized
  real(8), parameter :: D_P     = -0.0048d0   !< \f$D^{\mathrm{P}}\f$ for spin-polarized

  contains

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_ex
  !--
  !> @brief Fortran function calculating the exchange energy for a paramagnetic system.
  !> @details The exchange energy \f$\varepsilon_{\mathrm{x}}\f$ is calculated in the Hartree atomic
  !!          unit using the following formula
  !!          \f[
  !!            \varepsilon_{\mathrm{x}}(r_{\mathrm{s}})=
  !!            -\frac{3}{4}\left(\frac{3}{2\pi}\right)^{\frac{2}{3}}\frac{1}{r_{\mathrm{s}}}.
  !!          \f]
  !> @author Shigeru Tsukamoto
  elemental function get_ex( rs ) result( ex )
    use constant_mod, only : PI
    implicit none
    !-- parameters
    real(8), parameter :: A = -3.0d0 / 4.0d0 * ( 3.0d0 / ( 2.0d0 * PI ) ) ** ( 2.0d0 / 3.0d0 )
    !-- arguments (input)
    real(8), intent(in) :: rs  !< Wigner-Seitz radius \f$r_{\mathrm{s}}\f$ defined in the module
                               !! function ::get_rs()
    !-- arguments (output)
    real(8) :: ex  !< exchange energy \f$\varepsilon_{\mathrm{x}}\f$ in Hartree atomic unit
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    ex = A / rs
    return
  end function get_ex
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_vx
  !--
  !> @brief Fortran function calculating the exchange potential for a paramagnetic system.
  !> @details The exchange potential \f$v_{\mathrm{x}}\f$ is calculated in the Hartree atomic unit
  !!          using the following formula
  !!          \f[
  !!            v_{\mathrm{x}}(r_{\mathrm{s}})
  !!            =-\left(\frac{3}{2\pi}\right)^{\frac{2}{3}}\frac{1}{r_{\mathrm{s}}}.
  !!          \f]
  !> @author Shigeru Tsukamoto
  elemental function get_vx( rs ) result( vx )
    use constant_mod, only : PI
    implicit none
    !-- parameters
    real(8), parameter :: A = - ( 3.0d0 / ( 2.0d0 * PI ) ) ** ( 2.0d0 / 3.0d0 )
    !-- arguments (input)
    real(8), intent(in) :: rs  !< Wigner-Seitz radius \f$r_{\mathrm{s}}\f$ defined in the module
                               !! function ::get_rs()
    !-- arguments (output)
    real(8) :: vx  !< exchange potential \f$v_{\mathrm{x}}\f$ in Hartree atomic unit
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    vx = A / rs
    return
  end function get_vx
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_ec_rslarge
  !--
  !> @brief Fortran function calculating the correlation energy for low charge density
  !> @details The correlation energy \f$\varepsilon_{\mathrm{c}}\f$ is calculated using the
  !!          following formula:
  !!          \f[
  !!            \varepsilon_{\mathrm{c}}=
  !!            \frac{\gamma}{1+\beta_{1}\sqrt{r_{\mathrm{s}}}+\beta_{2}r_{\mathrm{s}}}
  !!          \f]
  !!          where \f$\gamma\f$, \f$\beta_{1}\f$, and \f$\beta_{2}\f$ are given in TABLE VII and
  !!          in Appendix C of Ref. @cite PRB23-005048.
  !> @author Shigeru Tsukamoto
  !--
  elemental function get_ec_rslarge( rs, gamma, beta1, beta2 ) result( ec )
    implicit none
    !-- arguments (input)
    real(8), intent(in) :: rs     !< Wigner-Seitz radius \f$r_{\mathrm{s}}\f$ defined in the module
                                  !! function ::get_rs()
    real(8), intent(in) :: gamma  !< parameter \f$\gamma\f$
    real(8), intent(in) :: beta1  !< parameter \f$\beta_{\mathrm{1}}\f$
    real(8), intent(in) :: beta2  !< parameter \f$\beta_{\mathrm{2}}\f$
    !-- argument (output)
    real(8) :: ec  !< correlation energy \f$\varepsilon_{\mathrm{c}}\f$ in Hartree atomic unit
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    ec = gamma / ( 1.0d0 + beta1 * sqrt( rs ) + beta2 * rs )
    return
  end function get_ec_rslarge
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_ec_rssmall
  !--
  !> @brief Fortran function calculating the correlation energy for high charge density
  !> @details The correlation energy \f$\varepsilon_{\mathrm{c}}\f$ is calculated using the
  !!          following formula:
  !!          \f[
  !!            \varepsilon_{\mathrm{c}}=
  !!            A \ln r_{\mathrm{s}} + B + C r_{\mathrm{s}} \ln r_{\mathrm{s}} + D r_{\mathrm{s}}
  !!          \f]
  !!          where \f$A\f$, \f$B\f$, \f$C\f$, and \f$D\f$ are given in TABLE VII and in Appendix C
  !!          of Ref. @cite PRB23-005048.
  !> @author Shigeru Tsukamoto
  !--
  elemental function get_ec_rssmall( rs, a, b, c, d ) result( ec )
    implicit none
    !-- arguments (input)
    real(8), intent(in) :: rs  !< Wigner-Seitz radius \f$r_{\mathrm{s}}\f$ defined in the module
                               !! function ::get_rs()
    real(8), intent(in) :: a   !< parameter \f$A\f$
    real(8), intent(in) :: b   !< parameter \f$B\f$
    real(8), intent(in) :: c   !< parameter \f$C\f$
    real(8), intent(in) :: d   !< parameter \f$D\f$
    !-- argument (output)
    real(8) :: ec  !< correlation energy \f$\varepsilon_{\mathrm{c}}\f$ in Hartree atomic unit
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    ec = a * log( rs ) + b + c * rs * log( rs ) + d * rs
    return
  end function get_ec_rssmall
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_vc_rslarge
  !--
  !> @brief Fortran function calculating the correlation potential for low charge density
  !> @details The correlation potential \f$v_{\mathrm{c}}\f$ is calculated using the
  !!          following formula:
  !!          \f[
  !!            v_{\mathrm{c}}=
  !!            \frac{\gamma}{(1+\beta_{1}\sqrt{r_{\mathrm{s}}}+\beta_{2}r_{\mathrm{s}})^{2}}
  !!            \left(1+\frac{7}{6}\beta_{1}\sqrt{r_{\mathrm{s}}}+\frac{4}{3}\beta_{2}r_{\mathrm{s}}\right)
  !!          \f]
  !!          where \f$\gamma\f$, \f$\beta_{1}\f$, and \f$\beta_{2}\f$ are given in TABLE VII and
  !!          in Appendix C of Ref. @cite PRB23-005048.
  !> @author Shigeru Tsukamoto
  !--
  elemental function get_vc_rslarge( rs, gamma, beta1, beta2 ) result( vc )
    implicit none
    !-- arguments (input)
    real(8), intent(in) :: rs     !< Wigner-Seitz radius \f$r_{\mathrm{s}}\f$ defined in the module
                                  !! function ::get_rs()
    real(8), intent(in) :: gamma  !< parameter \f$\gamma\f$
    real(8), intent(in) :: beta1  !< parameter \f$\beta_{\mathrm{1}}\f$
    real(8), intent(in) :: beta2  !< parameter \f$\beta_{\mathrm{2}}\f$
    !-- argument (output)
    real(8) :: vc  !< correlation potential \f$v_{\mathrm{c}}\f$ in Hartree atomic unit
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    vc = gamma / ( 1.0d0 + beta1 * sqrt( rs ) + beta2 * rs ) ** 2 &
   &   * ( 1.0d0 + 7.0d0 / 6.0d0 * beta1 * sqrt( rs ) + 4.0d0 / 3.0d0 * beta2 * rs )
    return
  end function get_vc_rslarge
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_vc_rssmall
  !--
  !> @brief Fortran function calculating the correlation potential for high charge density
  !> @details The correlation potential \f$v_{\mathrm{c}}\f$ is calculated using the
  !!          following formula:
  !!          \f[
  !!            v_{\mathrm{c}}=
  !!            A \ln r_{\mathrm{s}} + B - \frac{1}{3} A + \frac{2}{3}C r_{\mathrm{s}}
  !!            \ln r_{\mathrm{s}} + \left( \frac{2}{3} D - \frac{1}{3} C \right) r_{\mathrm{s}}
  !!          \f]
  !!          where \f$A\f$, \f$B\f$, \f$C\f$, and \f$D\f$ are given in TABLE VII and in Appendix C
  !!          of Ref. @cite PRB23-005048.
  !> @author Shigeru Tsukamoto
  !--
  elemental function get_vc_rssmall( rs, a, b, c, d ) result( vc )
    implicit none
    !-- arguments (input)
    real(8), intent(in) :: rs  !< Wigner-Seitz radius \f$r_{\mathrm{s}}\f$ defined in the module
                               !! function ::get_rs()
    real(8), intent(in) :: a   !< parameter \f$A\f$
    real(8), intent(in) :: b   !< parameter \f$B\f$
    real(8), intent(in) :: c   !< parameter \f$C\f$
    real(8), intent(in) :: d   !< parameter \f$D\f$
    !-- argument (output)
    real(8) :: vc  !< correlation energy \f$\varepsilon_{\mathrm{c}}\f$ in Hartree atomic unit
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    vc = a * log( rs ) + b - 1.0d0 / 3.0d0 * a + 2.0d0 / 3.0d0 * c * rs * log( rs ) &
   &   + ( 2.0d0 / 3.0d0 * d - 1.0d0 / 3.0d0 * c ) * rs
    return
  end function get_vc_rssmall
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_rs
  !--
  !> @brief Fortran function calculating the Wigner-Seitz radius.
  !> @details The Wigner-Seitz radius \f$r_{\mathrm{s}}\f$ is defined as
  !!          \f[r_{\mathrm{s}}=\left(\frac{3}{4\pi\rho}\right)^{\frac{1}{3}}\f]
  !!          where \f$\rho\f$ is charge density.
  !> @author Shigeru Tsukamoto
  !--
  elemental function get_rs( rho ) result( rs )
    use constant_mod, only : PI
    implicit none
    !-- parameters
    real(8), parameter :: A = 3.0d0 / ( 4.0d0 * PI )
    real(8), parameter :: ONETHIRD = 1.0d0 / 3.0d0
    !-- arguments (input)
    real(8), intent(in) :: rho  !< charge density \f$\rho\f$
    !-- arguments (output)
    real(8) :: rs  !< Wigner-Seitz radius \f$r_{\mathrm{s}}\f$ in the atomic unit
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    rs = ( A / max( rho, RHOMIN ) ) ** ONETHIRD
    return
  end function get_rs
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_zeta
  !--
  !> @brief Fortran function returning relative spin polarization
  !> @details The relative spin polarization \f$\zeta\f$ is calculated as
  !!          \f[\zeta=\frac{\rho_{\uparrow}-\rho_{\downarrow}}{\rho_{\uparrow}+\rho_{\downarrow}}\f]
  !!          In order to avoid invalid arithmetic operation, \f$\zeta\f$ is kept to be
  !!          \f$-1+\delta\zeta<\zeta<1-\delta\zeta\f$ (\f$\delta\zeta=1\times 10^{-16}\f$).
  !> @author Shigeru Tsukamoto
  !--
  elemental function get_zeta( up, dn ) result( zeta )
    implicit none
    !-- parameters
    real(8), parameter :: ZETADELTA = 1.0d-16
    real(8), parameter :: ZETAMAX = 1.0d0 - ZETADELTA
    real(8), parameter :: ZETAMIN = ZETADELTA - 1.0d0
    !-- arguments(input)
    real(8), intent(in) :: up  !< up-spin electron density \f$\rho_{\uparrow}\f$
    real(8), intent(in) :: dn  !< down-spin electron density \f$\rho_{\downarrow}\f$
    !-- return values
    real(8) :: zeta  !< relative spin polarization \f$\zeta\f$
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    zeta = min( max( ( up - dn ) / ( up + dn ), ZETAMIN ), ZETAMAX )
    return
  end function get_zeta
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_fzeta
  !--
  !> @brief Fortran function returning spin-polarization contribution
  !> @details The contribution \f$f(\zeta)\f$ is calculated as
  !!          \f[f(\zeta)=\frac{(1+\zeta)^{4/3}+(1-\zeta)^{4/3}-2}{2^{4/3}-2}\f]
  !!          where \f$\zeta\f$ is the relative spin polarization, and defined as
  !!          \f[\zeta=\frac{\rho_{\uparrow}-\rho_{\downarrow}}{\rho_{\uparrow}+\rho_{\downarrow}}\f]
  !> @author Shigeru Tsukamoto
  !--
  elemental function get_fzeta( zeta ) result( fzeta )
    implicit none
    !-- parameters
    real(8), parameter :: FOURTHIRD = 4.0d0 / 3.0d0
    real(8), parameter :: A = 1.0d0 / ( 2.0d0 ** FOURTHIRD - 2.0d0 )
    !-- arguments (input)
    real(8), intent(in) :: zeta  !< relative spin polarization \f$\zeta\f$
    !-- arguments (output)
    real(8) :: fzeta  !< function value \f$f(\zeta)\f$
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    fzeta = ( ( 1.0d0 + zeta ) ** FOURTHIRD + ( 1.0d0 - zeta ) ** FOURTHIRD - 2.0d0 ) * A
    return
  end function get_fzeta
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- get_dfdzeta
  !--
  !> @brief Fortran function returning the derivative of spin-polarization contribution
  !> @details The derivative \f$\mathrm{d}f(\zeta)/\mathrm{d}\zeta\f$ is calculated as
  !!          \f[
  !!            \frac{\mathrm{d}}{\mathrm{d}\zeta}f(\zeta)=
  !!            \frac{4[(1+\zeta)^{1/3}-(1-\zeta)^{1/3}]}{3(2^{4/3}-2)}\f]
  !!          where \f$\zeta\f$ is the relative spin polarization, and defined as
  !!          \f[\zeta=\frac{\rho_{\uparrow}-\rho_{\downarrow}}{\rho_{\uparrow}+\rho_{\downarrow}}\f]
  !!          The function \f$f(\zeta)\f$ is defined in ::get_fzeta().
  !> @author Shigeru Tsukamoto
  !--
  elemental function get_dfdzeta( zeta ) result( dfdzeta )
    implicit none
    !-- parameters
    real(8), parameter :: ONETHIRD = 1.0d0 / 3.0d0  !< \f$1/3\f$
    real(8), parameter :: FOURTHIRD = 4.0d0 / 3.0d0  !< \f$4/3\f$
    real(8), parameter :: A = FOURTHIRD / ( 2.0d0 ** FOURTHIRD - 2.0d0 ) !< \f$4/3(2^{4/3}-2)\f$
    !-- arguments (input)
    real(8), intent(in) :: zeta  !< relative spin polarization \f$\zeta\f$
    !-- arguments (output)
    real(8) :: dfdzeta  !< function value \f$\mathrm{d}f/\mathrm{d}\zeta\f$
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    dfdzeta = ( ( 1.0d0 + zeta ) ** ONETHIRD - ( 1.0d0 - zeta ) ** ONETHIRD ) * A
    return
  end function get_dfdzeta
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- xcpz81_set_exchange_unpolarized( ex, vx, rho )
  !--
  !> @brief Fortran subroutine calculating the exchange functionals for a non-spin-polarized system
  !> @details The exchange-energy density \f$\varepsilon_{\mathrm{x}}(r_{\mathrm{s}})\f$ is
  !!          calculated by ::get_ex(), and the exchange potential
  !!          \f$v_{\mathrm{x}}(r_{\mathrm{s}})\f$ is calculated by ::get_vx().
  !!          \f$r_{\mathrm{s}}\f$ is the Wigner-Seitz radius that is
  !!          calculated from the electron density \f$\rho\f$ by ::get_rs().
  !> @author Shigeru Tsukamoto
  !--
  module procedure xcpz81_set_exchange_unpolarized
    implicit none
    !-- local variables
    real(8) :: ro, rs
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    !-- initialize
    ro = max( rho, RHOMIN )
    rs = get_rs( rho=ro )
    !-- calculate exchange-energy density
    ex = get_ex( rs=rs )
    !-- calculate exchange potential
    vx = get_vx( rs=rs )
    return
  end procedure xcpz81_set_exchange_unpolarized
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- xcpz81_set_exchange_polarized( ex, vx1, vx2, rho1, rho2 )
  !--
  !> @brief Fortran subroutine calculating the exchange functionals for a spin-polarized system
  !> @details The exchange-energy density \f$\varepsilon_{\mathrm{x}}(r_{\mathrm{s}},\zeta)\f$ is
  !!          calculated as
  !!          \f[
  !!            \varepsilon_{\mathrm{x}}(r_{\mathrm{s}},\zeta)=\varepsilon_{\mathrm{x}}
  !!            (r_{\mathrm{s}})[1+(2^{1/3}-1)f(\zeta)]
  !!          \f]
  !!          where \f$r_{\mathrm{s}}\f$ is the Wigner-Seitz radius calculated from the electron
  !!          density \f$\rho\f$ by ::get_rs(), and \f$\zeta\f$ is the relative spin polarization
  !!          defined as
  !!          \f[\zeta=\frac{\rho_{\uparrow}-\rho_{\downarrow}}{\rho_{\uparrow}+\rho_{\downarrow}}.\f]
  !!          \f$\varepsilon_{\mathrm{x}}(r_{\mathrm{s}})\f$ represents the exchange-energy density
  !!          of a non-spin-polarized system, and is calculated by ::get_ex().
  !!          \f$f(\zeta)\f$ represents spin-polarization contribution and is calculated by
  !!          ::get_fzeta().
  !!          The exchange potentials \f$v_{\mathrm{x},\uparrow}(r_{\mathrm{s}},\zeta)\f$ and
  !!          \f$v_{\mathrm{x},\downarrow}(r_{\mathrm{s}},\zeta)\f$ are calculated as
  !!          \f[
  !!            v_{\mathrm{x},\uparrow(\downarrow)}(r_{\mathrm{s}},\zeta)=
  !!            v_{\mathrm{x}}(r_{\mathrm{s}})[1+(2^{1/3}-1)f(\zeta)]\pm 2
  !!            \varepsilon_{\mathrm{x}}(r_{\mathrm{s}})(2^{1/3}-1)\frac{\mathrm{d}f(\zeta)}
  !!            {\mathrm{d}\zeta}\frac{\rho_{\downarrow(\uparrow)}}
  !!            {\rho_{\uparrow}+\rho_{\downarrow}}.
  !!          \f]
  !!          \f$v_{\mathrm{x}}(r_{\mathrm{s}})\f$ represents the exchange potential of a
  !!          non-spin-polarized system, and is calculated by ::get_vx(). The derivative of the
  !!          spin-polarization contribution \f$\mathrm{d}f(\zeta)/\mathrm{d}\zeta\f$ is calculated
  !!          by ::get_dfdzeta().
  !> @author Shigeru Tsukamoto
  !--
  module procedure xcpz81_set_exchange_polarized
    implicit none

    !-- parameters
    real(8), parameter :: A = 2.0d0 ** ( 1.0d0 / 3.0d0 ) - 1.0d0  !< \f$2^{1/3}-1\f$

    !-- local variables
    real(8) :: up, dn, rho, rs, zeta, fzeta, dfdzeta
    real(8) :: exu, vxu

    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

    !-- initialize
    up      = max( rho1, RHOMIN )
    dn      = max( rho2, RHOMIN )
    rho     = up + dn
    rs      = get_rs( rho=rho )
    zeta    = get_zeta( up=up, dn=dn )
    fzeta   = get_fzeta( zeta=zeta )
    dfdzeta = get_dfdzeta( zeta=zeta )

    !-- calculate exchange-energy density
    exu = get_ex( rs=rs )
    ex  = exu * ( 1.0d0 + A * fzeta )

    !-- calculate exchange potentials
    vxu  = get_vx( rs=rs )
    vx1 = vxu * ( 1.0d0 + A * fzeta ) + 2.0d0 * exu * A * dfdzeta * dn / rho
    vx2 = vxu * ( 1.0d0 + A * fzeta ) - 2.0d0 * exu * A * dfdzeta * up / rho

    return
  end procedure xcpz81_set_exchange_polarized
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- xcpz81_set_correlation_unpolarized( ec, vc, rho )
  !--
  !> @brief Fortran subroutine calculating the correlation functionals for a non-spin-polarized system
  !> @details The correlation-energy density \f$\varepsilon_{\mathrm{c}}(r_{\mathrm{s}})\f$ is
  !!          calculated as
  !!          \f[
  !!            \varepsilon_{\mathrm{c}}(r_{\mathrm{s}})=
  !!            \begin{cases}
  !!              \varepsilon_{\mathrm{c}}(r_{\mathrm{s}},\gamma,\beta_{1},\beta_{2}) & \text{for
  !!              $r_{\mathrm{s}}>1$} \\
  !!              \varepsilon_{\mathrm{c}}(r_{\mathrm{s}},a,b,c,d) & \text{for $r_{\mathrm{s}}<1$}
  !!            \end{cases}
  !!          \f]
  !!          where \f$r_{\mathrm{s}}\f$ is the Wigner-Seitz radius, and calculated from the electron
  !!          density \f$\rho\f$ by ::get_rs().
  !!          \f$\varepsilon_{\mathrm{c}}(r_{\mathrm{s}},\gamma,\beta_{1},\beta_{2})\f$ and
  !!          \f$\varepsilon_{\mathrm{c}}(r_{\mathrm{s}},a,b,c,d)\f$ are calculated by
  !!          ::get_ec_rslarge() and ::get_ec_rssmall(), respectively. The parameters \f$\gamma\f$,
  !!          \f$\beta_{1}\f$, \f$\beta_{2}\f$, \f$a\f$, \f$b\f$, \f$c\f$, and \f$d\f$ are given
  !!          as #gamma_u, #beta1_u, #beta2_u, #a_u, #b_u, #c_u, and #d_u, respectively.
  !!          The correlation potential \f$v_{\mathrm{c}}(r_{\mathrm{s}})\f$ is calculated as
  !!          \f[
  !!            v_{\mathrm{c}}(r_{\mathrm{s}})=
  !!            \begin{cases}
  !!              v_{\mathrm{c}}(r_{\mathrm{s}},\gamma,\beta_{1},\beta_{2}) & \text{for $r_{\mathrm{s}}>1$} \\
  !!              v_{\mathrm{c}}(r_{\mathrm{s}},a,b,c,d) & \text{for $r_{\mathrm{s}}<1$}
  !!            \end{cases}
  !!          \f]
  !!          \f$v_{\mathrm{c}}(r_{\mathrm{s}},\gamma,\beta_{1},\beta_{2})\f$ and
  !!          \f$v_{\mathrm{c}}(r_{\mathrm{s}},a,b,c,d)\f$ are calculated by
  !!          ::get_vc_rslarge() and ::get_vc_rssmall() with the parameters mentioned above,
  !!          respectively.
  !> @author Shigeru Tsukamoto
  !--
  module procedure xcpz81_set_correlation_unpolarized
    implicit none
    !-- local variables
    real(8) :: ro, rs
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    !-- initialize
    ro = max( rho, RHOMIN )
    rs = get_rs( rho=ro )
    !-- calculate correlation-energy density
    if( rs > 1.0d0 )then  !-- large rs: low density
      ec = get_ec_rslarge( rs=rs, gamma=GAMMA_U, beta1=BETA1_U, beta2=BETA2_U )
    else                  !-- small rs: high density
      ec = get_ec_rssmall( rs=rs, a=A_U, b=B_U, c=C_U, d=D_U )
    end if  !-- rs
    !-- calculate correlation potential
    if( rs > 1.0d0 )then  !-- large rs: low density
      vc = get_vc_rslarge( rs=rs, gamma=GAMMA_U, beta1=BETA1_U, beta2=BETA2_U )
    else                  !-- small rs: high density
      vc = get_vc_rssmall( rs=rs, a=A_U, b=B_U, c=C_U, d=D_U )
    end if  !-- rs
    return
  end procedure xcpz81_set_correlation_unpolarized
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- xcpz81_set_correlation_polarized( ec, vc1, vc2, rho1, rho2 )
  !--
  !> @brief Fortran subroutine calculating the correlation functionals for a spin-polarized system
  !> @details The correlation-energy density \f$\varepsilon_{\mathrm{c}}(r_{\mathrm{s}},\zeta)\f$ is
  !!          calculated as
  !!          \f[
  !!            \varepsilon_{\mathrm{c}}(r_\mathrm{s},\zeta)=
  !!            \varepsilon_{\mathrm{c}}(r_\mathrm{s},0)[1-f(\zeta)]+
  !!            \varepsilon_{\mathrm{c}}(r_\mathrm{s},1)f(\zeta)
  !!          \f]
  !!          where \f$r_{\mathrm{s}}\f$ is the Wigner-Seitz radius calculated from the electron
  !!          density \f$\rho\f$ by ::get_rs(), and \f$\zeta\f$ is the relative spin polarization
  !!          defined as
  !!          \f[\zeta=\frac{\rho_{\uparrow}-\rho_{\downarrow}}{\rho_{\uparrow}+\rho_{\downarrow}}.\f]
  !!          \f$\varepsilon_{\mathrm{c}}(r_\mathrm{s},0)\f$ and
  !!          \f$\varepsilon_{\mathrm{c}}(r_\mathrm{s},1)\f$ represent the correlation-energy
  !!          density for unpolarized and fully-polarized systems, respectively. They are defined as
  !!          \f[
  !!            \varepsilon_{\mathrm{c}}(r_{\mathrm{s}},\zeta)=
  !!            \begin{cases}
  !!              \varepsilon_{\mathrm{c}}(r_{\mathrm{s}},\gamma,\beta_{1},\beta_{2}) & \text{for
  !!              $r_{\mathrm{s}}>1$} \\
  !!              \varepsilon_{\mathrm{c}}(r_{\mathrm{s}},a,b,c,d) & \text{for $r_{\mathrm{s}}<1$}
  !!            \end{cases}
  !!          \f]
  !!          \f$\varepsilon_{\mathrm{c}}(r_{\mathrm{s}},\gamma,\beta_{1},\beta_{2})\f$ and
  !!          \f$\varepsilon_{\mathrm{c}}(r_{\mathrm{s}},a,b,c,d)\f$ are calculated by
  !!          ::get_ec_rslarge() and ::get_ec_rssmall(), respectively.
  !!          The parameters \f$\gamma\f$, \f$\beta_{1}\f$, \f$\beta_{2}\f$, \f$a\f$, \f$b\f$,
  !!          \f$c\f$, and \f$d\f$ for \f$\zeta=0\f$ are given as #gamma_u, #beta1_u, #beta2_u,
  !!          #a_u, #b_u, #c_u, and #d_u, respectively. Those for \f$\zeta=1\f$ are as #gamma_p,
  !!          #beta1_p, #beta2_p, #a_p, #b_p, #c_p, and #d_p.
  !!          The correlation potentials \f$v_{\mathrm{c},\uparrow}(r_{\mathrm{s}},\zeta)\f$ and
  !!          \f$v_{\mathrm{c},\downarrow}(r_{\mathrm{s}},\zeta)\f$ are calculated as
  !!          \f[
  !!            v_{\mathrm{c},\uparrow(\downarrow)}(r_{\mathrm{s}},\zeta)=
  !!            v_{\mathrm{c}}(r_{\mathrm{s}},1)f(\zeta)+v_{\mathrm{c}}(r_{\mathrm{s}},0)[1-f(\zeta)]
  !!            \pm 2[\varepsilon_{\mathrm{c}}(r_{\mathrm{s}},1)-\varepsilon_{\mathrm{c}}
  !!            (r_{\mathrm{s}},0)]\frac{\mathrm{d}f(\zeta)}{\mathrm{d}\zeta}\frac{\rho_{\downarrow
  !!            (\uparrow)}}{\rho_{\uparrow}+\rho_{\downarrow}}.
  !!          \f]
  !!          \f$v_{\mathrm{c}}(r_\mathrm{s},0)\f$ and \f$v_{\mathrm{c}}(r_\mathrm{s},1)\f$
  !!          represent the correlation potentials for unpolarized and fully-polarized systems,
  !!          respectively. They are defined as
  !!          \f[
  !!            v_{\mathrm{c}}(r_{\mathrm{s}},\zeta)=
  !!            \begin{cases}
  !!              v_{\mathrm{c}}(r_{\mathrm{s}},\gamma,\beta_{1},\beta_{2}) & \text{for
  !!              $r_{\mathrm{s}}>1$} \\
  !!              v_{\mathrm{c}}(r_{\mathrm{s}},a,b,c,d) & \text{for $r_{\mathrm{s}}<1$}
  !!            \end{cases}
  !!          \f]
  !!          \f$v_{\mathrm{c}}(r_{\mathrm{s}},\gamma,\beta_{1},\beta_{2})\f$ and
  !!          \f$v_{\mathrm{c}}(r_{\mathrm{s}},a,b,c,d)\f$ are calculated by
  !!          ::get_vc_rslarge() and ::get_vc_rssmall(), respectively.
  !!          \f$\mathrm{d}f(\zeta)/\mathrm{d}\zeta\f$ is calculated by ::get_dfdzeta().
  !> @author Shigeru Tsukamoto
  !--
  module procedure xcpz81_set_correlation_polarized
    implicit none

    !-- local variables
    real(8) :: up, dn, rho, rs, zeta, fzeta, dfdzeta
    real(8) :: ecu, ecp, vcu, vcp

    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

    !-- initialize
    up      = max( rho1, RHOMIN )
    dn      = max( rho2, RHOMIN )
    rho     = up + dn
    rs      = get_rs( rho=rho )
    zeta    = get_zeta( up=up, dn=dn )
    fzeta   = get_fzeta( zeta=zeta )
    dfdzeta = get_dfdzeta( zeta=zeta )

    !-- calculate correlation-energy density
    if( rs > 1.0d0 )then  !-- large rs: low density
      ecu = get_ec_rslarge( rs=rs, gamma=GAMMA_U, beta1=BETA1_U, beta2=BETA2_U )  !< non spin polarized
      ecp = get_ec_rslarge( rs=rs, gamma=GAMMA_P, beta1=BETA1_P, beta2=BETA2_P )  !< full spin polarized
    else                  !-- small rs: high density
      ecu = get_ec_rssmall( rs=rs, a=A_U, b=B_U, c=C_U, d=D_U )  !< non spin polarized
      ecp = get_ec_rssmall( rs=rs, a=A_P, b=B_P, c=C_P, d=D_P )  !< full spin polarized
    end if  !-- rs
    ec = ecu + ( ecp - ecu ) * fzeta

    !-- calculate correlation potential
    if( rs > 1.0d0 )then  !-- large rs: low density
      vcu = get_vc_rslarge( rs=rs, gamma=GAMMA_U, beta1=BETA1_U, beta2=BETA2_U )
      vcp = get_vc_rslarge( rs=rs, gamma=GAMMA_P, beta1=BETA1_P, beta2=BETA2_P )
    else                  !-- small rs: high density
      vcu = get_vc_rssmall( rs=rs, a=A_U, b=B_U, c=C_U, d=D_U )
      vcp = get_vc_rssmall( rs=rs, a=A_P, b=B_P, c=C_P, d=D_P )
    end if  !-- rs
    vc1 = vcu + ( vcp - vcu ) * fzeta + 2.0d0 * ( ecp - ecu ) * dfdzeta * dn / rho
    vc2 = vcu + ( vcp - vcu ) * fzeta - 2.0d0 * ( ecp - ecu ) * dfdzeta * up / rho

    return
  end procedure xcpz81_set_correlation_polarized
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

end submodule xcpz81_smod
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
