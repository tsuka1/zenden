
! #define DEBUG

!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!> @file vcore_mod.F90
!> @brief Fortran module providing interfaces to the subprograms of the core-electron interaction
!> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
!> @copyright &copy; 2023- Shigeru Tsukamoto. All rights reserved.
!--
module vcore_mod
  implicit none
  private

  !-- scope & interface to vcore_get
  public :: vcore_get
  interface vcore_get
    module elemental function vcore_get( z, r ) result( v )
      integer, intent(in) :: z !< atomic number of the nucleus \f$z\f$
      real(8), intent(in) :: r !< distance from the nucleus \f$r\f$
      real(8) :: v !< core-electron potential
    end function vcore_get
  end interface vcore_get

  !-- scope & interface to vcore_export
  public :: vcore_export
  interface vcore_export
    module subroutine vcore_export( r, vcore, filename )
      use grid_mod, only : grid
      implicit none
      type(grid), intent(in) :: r !< grid instance related to the ion-core potential
      real(8), intent(in) :: vcore(:) !< core-electron potential
      character(len=*), optional, intent(in) :: filename !< name of file to be exported
    end subroutine vcore_export
  end interface vcore_export

end module vcore_mod
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
