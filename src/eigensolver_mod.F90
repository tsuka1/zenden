
! #define DEBUG

!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!> @file eigensolver_mod.F90
!> @brief Fortran module defining interface to switching subprogram to various eigensolvers
!> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
!> @copyright &copy; 2023- Shigeru Tsukamoto. All rights reserved.
!--
module eigensolver_mod
  use grid_mod, only : grid
  use hamiltonian_mod, only : hamiltonian
  implicit none
  private

  !-- scope & interface to eigensolver_solve
  public :: eigensolver_solve
  interface eigensolver_solve
    module subroutine eigensolver_solve( mode, r, h, neig, val, vec )
      implicit none
      character(len=*), intent(in) :: mode !< solver mode
      type(grid), intent(in) :: r !< radial grids
      type(hamiltonian), intent(inout) :: h !< Hamiltonian matrices
      integer, intent(in) :: neig !< number of eigenvalues
      real(8), intent(out) :: val(neig) !< eigenvalues
      real(8), intent(out) :: vec(h%n,neig) !< eigenvectors
    end subroutine eigensolver_solve
  end interface eigensolver_solve

end module eigensolver_mod
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
