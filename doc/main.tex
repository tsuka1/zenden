%-- document class
%\documentclass[12pt,a4paper,onecolumn,oneside]{jarticle}
\documentclass[12pt,a4paper,onecolumn,oneside]{article}

%-- packages
\usepackage[numbers]{natbib}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{geometry}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{mathtools}
\usepackage{layout}
\usepackage{braket}
\usepackage{fancyhdr}
\usepackage{lastpage}
%\usepackage{url}
\usepackage{listings}
%\usepackage{jvlisting}
\usepackage{plistings}
\usepackage{multirow}
\usepackage{tablefootnote}
\usepackage{verbatim}
\usepackage{here}
\usepackage{tcolorbox}
\usepackage{comment}
\usepackage{ascmac}
\usepackage{siunitx}
\usepackage{mhchem}
\usepackage{inputenc}
\usepackage{hyperref}
\usepackage{breakurl}  % The package "breakurl" should be used when you use the package "hyperref"
\usepackage{algorithm}
\usepackage{algpseudocode}

%-- layout setting
\geometry{top=0.75in,bottom=0.75in,left=0.5in,right=0.5in}
\setlength{\parindent}{0mm}
\setlength{\parskip}{0.5\baselineskip}
\pagestyle{fancy}
\fancyhf{}
%\fancyhead[R]{\textbf{The performance of new graduates}}
\fancyhead[R]{\leftmark}
%\fancyhead[L]{\rightmark}
%\fancyfoot[C]{--~\thepage~--}
\fancyfoot[C]{--~\thepage~/~\pageref{LastPage}~--}% the label is defined in the style file lastpage.sty
\renewcommand{\headrulewidth}{0.2pt}
\renewcommand{\footrulewidth}{0.2pt}
\fancypagestyle{firststyle}
{
  \fancyhf{}
  %\fancyfoot[C]{\footnotesize Page \thepage\ of \pageref{LastPage}}
  \fancyfoot[C]{--~\thepage~--}
}


%-- listing setting
\lstdefinestyle{mystyle}{
  language={Python},
  backgroundcolor={\color[gray]{0.95}},
  basicstyle={\small\ttfamily},
  keywordstyle={\small\bfseries},
  commentstyle={\small\rmfamily},
  columns=[l]{fullflexible},
  breaklines=true,
  numberstyle={\scriptsize\color[gray]{0.5}},
  numbers=left,
  numbersep=5pt,
  keepspaces=true,
  belowskip=0pt
}
\lstset{style=mystyle}
\renewcommand{\ttdefault}{pcr}

%-- new environment after lstlisting
\newenvironment{smallverbatim}{%
  \topsep=0pt\partopsep=0pt\small\verbatim%
}{%
  \endverbatim%
}%

%-- bibliography setting
%\bibliographystyle{unsrtnat}
%\bibliographystyle{hunsrt}
\bibliographystyle{utphys}

%-- algorithm
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}

%-- proof environment
\renewcommand{\proofname}{\textbf{証明}}
\renewcommand{\qedsymbol}{$\blacksquare$}
%\renewcommand{\qedsymbol}{\textbf{証明終わり}}

%-- definition environment
\theoremstyle{definition}
\newtheorem{theorem}{定理}
\newtheorem*{theorem*}{定理}
\newtheorem{definition}[theorem]{定義}
\newtheorem*{definition*}{定義}

%-- reference environment
\renewcommand{\refname}{参考文献}
\renewcommand{\bibname}{参考文献}

%-- command
\renewcommand{\emph}[1]{\textbf{#1}}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
%\newcommand{\mat}[1]{\boldsymbol{\mathrm{#1}}}
\newcommand{\mat}[1]{\mathbf{#1}}
\renewcommand{\hat}[1]{\widehat{#1}}
\renewcommand{\tilde}[1]{\widetilde{#1}}
%\newcommand*{\comment}[2]{\rlap{\raisebox{2ex}{\scriptsize \textcolor{red}{#2}}}{\textcolor{blue}{#1}}}


%% \maketitle
\makeatletter
\def\webaddress#1{\def\@webaddress{#1}}
\def\emailaddress#1{\def\@emailaddress{#1}}
\renewcommand{\@maketitle}{
\newpage
% \null
% \vskip 2em
\begin{center}
  \ifdefined\@webaddress%
    {\LARGE \@title\footnote{URL: \protect\url{\@webaddress}}}
  \else
    {\LARGE \@title}
  \fi
  \vskip 0.5em
  \ifdefined\@author
    \ifdefined\@emailaddress
      {\large \lineskip .5em
      \begin{tabular}[t]{cc}
        \@author & (E-Mail: \@emailaddress)
      \end{tabular}\par}
    \else
      {\large \lineskip .5em
      \begin{tabular}[t]{c}
        \@author
      \end{tabular}\par}
    \fi
  \fi
  \vskip 0.5em {\large \@date}
  \vskip 0.5em {\scriptsize \copyright\ 2024--\ \@author. All rights reserved.}
\end{center}
% \par
% \vskip 0.5em
\thispagestyle{empty}
\noindent\hrulefill
}
\makeatother


%-- bibliography style setting
\bibliographystyle{plain}

%-- title page setting
\title{Numerical Solution of Radial Kohn-Sham Equation}
%\webaddress{https://github.com/dc1394/hydrogen_fem}
\author{Shigeru Tsukamoto}
\emailaddress{tsukamoto.shigeru@gmail.com}
\date{\today}
%--
%-- document
%--
\begin{document}
\maketitle


\section{Radial Kohn-Sham equation on logarithmic grids}
The Kohn-Sham equation, which satisfies the radial component of wave function representing an electronic states of an atom, $R(r)$ with a radial coordinate $r$, is
\begin{equation}
\left(-\frac{1}{2}\frac{\mathrm{d}^2}{\mathrm{d}r^2}-\frac{Z}{r}+\frac{\ell(\ell+1)}{2r^2}\right)\chi(r)=E\chi(r),\quad\chi(r)=rR(r).
\label{eq:SchrodingerEq}
\end{equation}
Here and hereafter, the Hartree atomic unit, where $\hbar=m=|e|=1$ and $4\pi\varepsilon_0=1$, is used.
In the Kohn-Sham equation above, $Z$、$\ell$、$E$ denote the nucleus positive charge of the atom, the orbital angular momentum quantum number, and the energy of an electronic state, respectively.
The first term of the left-hand side represent the kinetic energy, the second term the attractive Coulomb interaction energy between electrons and the nucleus, and the third term the energy originating from the centrifugal force of electrons in an electronic orbital.
As seen in Eq.~\eqref{eq:SchrodingerEq}, the second and third terms have singular point at the origin $r=0$, and in its proximity the two terms widely vary with the radial coordinate $r$.
In order to obtain the eigenvalue $E$ and its eigenfunction $\chi(r)$ by means of numerical calculations, it is necessary to discretize Eq.~\eqref{eq:SchrodingerEq} along the radial coordinate $r$.
The radial-grid intervals obtained by the discretization are required so as to sample the rapid variation of the second and third terms of Eq.~\eqref{eq:SchrodingerEq} at around the origin $r=0$ as accurately as possible.
On the other hand, at the radial grids very distant from the singular point, the variation of the second and third terms is very gentle, and therefore, the radial-grid intervals should be not so fine as those close to the singular point.
Considering the effectiveness of the numerical calculations, the radial grids would be better to be dense at around the origin, and to be course at far away from the origin.\cite{ANarita1998}

To realize such discretization with non-uniform grid intervals on the radial coordinate, we here employ a logarithmic coordinate system.
The logarithmic coordinate $x$ is obtained using the following transform formula:
\begin{equation}
x=\ln r,
\label{eq:ChangeVariable}
\end{equation}
i.e., grid point in the logarithmic coordinate system $x$ is given as the logarithm of a radial grid $r$.
When the $x$-coordinate is sampled equidistantly, one can obtain the dense radial grid points in the proximity of the singular point, and course ones at far away from the origin.
Let's us apply this variable transformation ($r\rightarrow x$) on Eq.~\eqref{eq:SchrodingerEq}.
From Eq.~\eqref{eq:ChangeVariable}, $r=e^x$ is obvious.
In addition, it is necessary to represent the second derivative $\mathrm{d}^2/\mathrm{d}r^2$ in Eq.~\eqref{eq:SchrodingerEq} by a second derivative with respect to $x$.
Before deriving the differential operator, let's us think the variable transformation ($r\rightarrow x$) of the first derivative of an arbitray function $f$ with respect to $r$, i.e., $\mathrm{d}f/\mathrm{d}r$:
\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}r}f=\frac{\mathrm{d}x}{\mathrm{d}r}\frac{\mathrm{d}}{\mathrm{d}x}f=\frac{1}{r}\frac{\mathrm{d}}{\mathrm{d}x}f=\frac{1}{e^x}\frac{\mathrm{d}}{\mathrm{d}x}f.
\end{equation}
In the transformation from the second equation to the third one, the derivative of Eq.~\eqref{eq:ChangeVariable} with respect to $r$ ($\frac{\mathrm{d}x}{\mathrm{d}r}=\frac{1}{r}$) was used.
Using the equation above, the second derivative of an arbitrary function $f$ with respect to $r$, i.e., $\mathrm{d}^2f/\mathrm{d}r^2$, can be represented by the derivative with respect to $x$ as below:
\begin{equation}
\frac{\mathrm{d}^2}{\mathrm{d}r^2}f = \frac{\mathrm{d}}{\mathrm{d}r}\left(\frac{\mathrm{d}}{\mathrm{d}r}f\right)
 = \frac{\mathrm{d}x}{\mathrm{d}r}\frac{\mathrm{d}}{\mathrm{d}x}\left(\frac{1}{e^x}\frac{\mathrm{d}}{\mathrm{d}x}f\right)
 = \frac{1}{e^x}\left(-\frac{1}{e^x}\frac{\mathrm{d}}{\mathrm{d}x}+\frac{1}{e^x}\frac{\mathrm{d}^2}{\mathrm{d}x^2}\right)f
 = \frac{1}{e^{2x}}\left(\frac{\mathrm{d}^2}{\mathrm{d}x^2}-\frac{\mathrm{d}}{\mathrm{d}x}\right)f
\end{equation}
Using the equation above, the variable transformation ($r\rightarrow x$) can be applied to the Kohn-Sham equation \eqref{eq:SchrodingerEq}, and we obtain
\begin{equation}
\left[-\frac{1}{2e^{2x}}\left(\frac{\mathrm{d}^2}{\mathrm{d}x^2}-\frac{\mathrm{d}}{\mathrm{d}x}\right)-\frac{Z}{e^x}+\frac{\ell(\ell+1)}{2e^{2x}}\right]\chi(x)=E\chi(x),\quad\chi(x)=e^xR(x).
\label{eq:SchrodingerEq2}
\end{equation}
This is the Kohn-Sham equation represented by means of the logarithmic coordinate $x$.

\section{Transformation to canonical form}
The second-order linear differential equation \eqref{eq:SchrodingerEq2} can be transformed into a differential equation which does not have any first-derivative term, and it is called the canonical form of a second-order differential equaiton.
Firstly, $-2e^{2x}$ is operated to the both sides of Eq.~\eqref{eq:SchrodingerEq2} from the left so as to remove the coefficient of the second-derivative term.
Then, the differential equation reads
\begin{equation}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}\chi(x)-\frac{\mathrm{d}}{\mathrm{d}x}\chi(x)+\left(2e^{2x}E+2e^xZ-\ell(\ell+1)\right)\chi(x)=0,\quad\chi(x)=e^xR(x).
\label{eq:SchrodingerEq3}
\end{equation}
Next, using the following transformation, one can transform Eq.~\eqref{eq:SchrodingerEq3} to a canonical form.
\begin{itembox}[l]{Transformation of a second-order linear differential equation into a canonical form}
A second-order linear ordinary differential equation represented in the following form
\begin{equation*}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}y(x)+P(x)\frac{\mathrm{d}}{\mathrm{d}x^2}y(x)+Q(x)y(x)=R(x)
\end{equation*}
can be transformed into an canonical form.
\begin{equation*}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}y_0(x)+I(x)y_0(x)=J(x).
\end{equation*}
Here
\begin{equation*}
I(x)=Q(x)-\frac{P'(x)}{2}-\frac{P(x)^2}{4},
\quad
J(x)=\frac{R(x)}{u(x)},
\quad
y(x)=u(x)y_0(x),
\quad
u(x)=\exp\left(-\!\int\!\frac{P(x)}{2}\mathrm{d}x\right).
\end{equation*}
$P'(x)$ denotes the derivative of the function $P(x)$ with respect to $x$, and is defined as
\begin{equation*}
P'(x)=\frac{\mathrm{d}}{\mathrm{d}x}P(x)
\end{equation*}
\end{itembox}
Following the tranformation, from Eq.~\eqref{eq:SchrodingerEq3} one can be changed to be
\begin{equation}
P(x)=-1,
\quad
Q(x)=2e^{2x}E+2e^xZ-\ell(\ell+1),
\quad
R(x)=0
\end{equation}
and
\begin{equation}
I(x)=2e^{2x}E+2e^xZ-\ell(\ell+1)-\frac{1}{4},
\quad
J(x)=0.
\end{equation}
As the result, the second-order linear differential equation \eqref{eq:SchrodingerEq3} is transformed into the following canonical form:
\begin{equation}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}\xi(x)+\left(2e^{2x}E+2e^xZ-\ell(\ell+1)-\frac{1}{4}\right)\xi(x)=0.
\label{eq:SchrodingerEq5}
\end{equation}
Here, the solution of the radial Kohn-Sham equation \eqref{eq:SchrodingerEq5} above, i.e., $\xi(x)$, is related to the solution of the original Kohn-Sham equation \eqref{eq:SchrodingerEq2}, i.e.,  $\chi(x)$, as
\begin{equation}
\chi(x)=u(x)\xi(x),
\quad
u(x)=\exp\left(\frac{1}{2}x\right).
\end{equation}
Besides, the relationship between the radial wave function $R(x)$ and the solution $\xi(x)$ is given as
\begin{equation}
\xi(x)=\exp\left(\frac{1}{2}x\right)R(x).
\end{equation}

\section{Discretization for numerical calculations}
For ease of further discussion in this section, let us firstly change the second-order differential equation \eqref{eq:SchrodingerEq5} so that the term with eigenenergy $E$ appears at the right-hand side of the equation, as seen in Eqs.~\eqref{eq:SchrodingerEq2} and \eqref{eq:SchrodingerEq}:
\begin{equation}
-\frac{1}{2}\frac{\mathrm{d}^2}{\mathrm{d}x^2}\xi(x)+\left(-e^xZ+\frac{\ell(\ell+1)}{2}+\frac{1}{8}\right)\xi(x)=e^{2x}E\xi(x).
\label{eq:SchrodingerEq6}
\end{equation}
To discretize the equation above, we apply the following central finite-difference formula
\begin{equation}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}f_i=\frac{f_{i-1}-2f_i+f_{i+1}}{d_x^2}=\frac{1}{d_x^2}f_{i-1}-\frac{2}{d_x^2}f_i+\frac{1}{d_x^2}f_{i+1} \label{eq:FiniteDifference2}
\end{equation}
to the second-derivative term in Eq.~\eqref{eq:SchrodingerEq6}.
Here, $f_i$ denotes the value of the function at the $i$-th grid point $x_i$ on the $x$ coordinate, and $d_x$ the interval between two adjacent grid points on the $x$ coordinate.
Substituting the central finite-difference formula \eqref{eq:FiniteDifference2} in Eq.~\eqref{eq:SchrodingerEq6}, one obtains
\begin{equation}
-\frac{1}{2d_x^2}\xi_{i-1}+\left(\frac{1}{d_x^2}-Ze^{x_i}+\frac{\ell(\ell+1)}{2}+\frac{1}{8}\right)\xi_i-\frac{1}{2d_x^2}\xi_{i+1}=Ee^{2x_i}\xi_i.
\label{eq:SchrodingerEqDiscrete}
\end{equation}
In matrix representation, it appears
\begin{multline}
\left[
\begin{array}{ccccccccc}
\ddots & \ddots & \ddots & \ddots       & \ddots   &              &        &        & \\
\cdots & 0      & \beta  & \alpha_{i-1} & \beta    & 0            & \cdots &        & \\
       & \cdots & 0      & \beta        & \alpha_i & \beta        & 0      & \cdots & \\
       &        & \cdots & 0            & \beta    & \alpha_{i+1} & \beta  & 0      & \cdots \\
       &        &        &              & \ddots   & \ddots       & \ddots & \ddots & \ddots
\end{array}
\right]\left[
\begin{array}{c}
\vdots \\
\xi_{i-1} \\
\xi_i \\
\xi_{i+1} \\
\vdots
\end{array}
\right]\\
=E\left[
\begin{array}{ccccccc}
 \ddots & \ddots & \ddots       &          &              &        &        \\
 \cdots & 0      & e^{2x_{i-1}} & 0        & \cdots       &        &        \\
        & \cdots & 0            & e^{2x_i} & 0            & \cdots &        \\
        &        & \cdots       & 0        & e^{2x_{i+1}} & 0      & \cdots \\
        &        &              &          & \ddots       & \ddots & \ddots
\end{array}
\right]\left[
\begin{array}{c}
\vdots \\
\xi_{i-1} \\
\xi_i \\
\xi_{i+1} \\
\vdots
\end{array}
\right],
\label{eq:SchrodingerEqMatrixInfinite}
\end{multline}
where
\begin{equation}
\alpha_i=\frac{1}{d_x^2}-Ze^{x_i}+\frac{\ell(\ell+1)}{2}+\frac{1}{8},
\quad
\beta=-\frac{1}{2d_x^2}.
\end{equation}
In order to solve the linear equation above in numerical calculation, the matrix dimension has to be finite.
That is, we need determine the boundary conditions of the solution $\xi_{i}$ at $x_{i}\ll 0$ and $x_{i}\gg 0$.
Note that $x_{i}\ll 0$ is at close proximity to the singular point of the Kohn-Sham equation \eqref{eq:SchrodingerEq}, and $x_{i}\gg 0$ corresponds to $r\gg 1$ on the radial coordinate.

According to Narita \cite{ANarita1998}, it is known that the solution decays $\xi(x)\rightarrow 0$ at $r$ far away from the singular point ($r\rightarrow+\infty$, that is, $x\rightarrow+\infty$).
Therefore, it is possible to assume $\xi_i=0,(i>N)$ at a large number $N$.
On the other hand, it is known that in close proximity to the singular point ($r\approx 0$, that is, $x\approx-\infty$), the solution $\xi(x)$ behaves like
\begin{equation}
\xi(x)\propto \exp\left[(\ell+\frac{1}{2})x\right].
\end{equation}
The values of the function $\xi(x)$ at $x=x_0$ and $x=x_1$, i.e., $\xi_{0}$ and $\xi_{1}$, are
\begin{equation}
\xi_0\propto\exp\left[(\ell+\frac{1}{2})x_0\right]\ \text{and}\ \xi_1\propto\exp\left[(\ell+\frac{1}{2})x_1\right],
\end{equation}
respectively.
Using the two equations above and $x_1=x_0+d_x$, the ratio of $\xi_0$ to $\xi_1$ is easily found to be
\begin{equation}
\xi_0/\xi_1=\exp\left[-(\ell+\frac{1}{2})d_x\right]\iff\xi_0=\exp\left[-\left(\ell+\frac{1}{2}\right)d_x\right]\xi_1.
\end{equation}
Substituting the equation in the row of $i=1$ in Eq.~\eqref{eq:SchrodingerEqMatrixInfinite}, the linear equation at $i=1$ becomes
\begin{equation}
\left(\beta\exp\left[-\left(\ell+\frac{1}{2}\right)d_x\right]+\alpha_1\right)\xi_1+\beta\xi_2=Ee^{2x_1}\xi_1,
\end{equation}
the term with $\xi_0$ can be removed.
For the row of $i=N$, using $\xi_i=0,(i>N)$, one can obtain
\begin{equation}
\beta\xi_{N-1}+\alpha_N\xi_N=Ee^{2x_N}\xi_N,
\end{equation}
and the terms of $i>N$ do not appear anymore.
In this way, it is succeeded in transforming the infinite-dimensional linear equations \eqref{eq:SchrodingerEqMatrixInfinite} into a $N$-dimensional linear equation, which can be solved in numerical calculation:
\begin{multline}
\left[
\begin{array}{ccccccc}
 \alpha_1' & \beta     & 0         & \cdots & \cdots        & \cdots        & 0      \\
 \beta     & \alpha_2' & \beta     & \ddots &               &               & \vdots \\
 0         & \beta     & \alpha_3' & \beta  & \ddots        &               & \vdots \\
 \vdots    & \ddots    & \ddots    & \ddots & \ddots        & \ddots        & \vdots \\
 \vdots    &           & \ddots    & \beta  & \alpha_{N-2}' & \beta         & 0 \\
 \vdots    &           &           & \ddots & \beta         & \alpha_{N-1}' & \beta \\
 0         & \cdots    & \cdots    & \cdots & 0             & \beta         & \alpha_N' \\
\end{array}
\right]\left[
\begin{array}{c}
\xi_1 \\
\xi_2 \\
\xi_3 \\
\vdots \\
\xi_{N-2} \\
\xi_{N-1} \\
\xi_N
\end{array}
\right]\\
=E\left[
\begin{array}{ccccccc}
 e^{2x_1} & 0        & \cdots   & \cdots & \cdots       & \cdots       & 0      \\
 0        & e^{2x_2} & \ddots   &        &              &              & \vdots \\
 \vdots   & \ddots   & e^{2x_3} & \ddots &              &              & \vdots \\
 \vdots   &          & \ddots   & \ddots & \ddots       &              & \vdots \\
 \vdots   &          &          & \ddots & e^{2x_{N-2}} & \ddots       & \vdots \\
 \vdots   &          &          &        & \ddots       & e^{2x_{N-1}} & 0      \\
 0        & \cdots   & \cdots   & \cdots & \cdots       & 0            & e^{2x_N} \\
\end{array}
\right]\left[
\begin{array}{c}
\xi_1 \\
\xi_2 \\
\xi_3 \\
\vdots \\
\xi_{N-2} \\
\xi_{N-1} \\
\xi_N
\end{array}
\right].
\label{eq:SchrodingerEqMatrixFinite}
\end{multline}
The non-zero elements in the matrix at the left-hand side are given as
%\begin{equation}
%\alpha_i'=
%\begin{cases}
%\beta\exp\left[-\left(\ell+\frac{1}{2}\right)d_x\right]+\alpha_1 & \text{for}\;i=1 \\
%\alpha_i                                                         & \text{otherwise}
%\end{cases},
%\quad
%\alpha_i=\frac{1}{d_x^2}-Ze^{x_i}+\frac{\ell(\ell+1)}{2}+\frac{1}{8},
%\quad
%\beta=-\frac{1}{2d_x^2}.
%\end{equation}
\begin{equation}
\alpha_i'=\left\{
\begin{aligned}
& \beta\exp\left[-\left(\ell+\frac{1}{2}\right)d_x\right]+\alpha_1 && \text{for}\;i=1 \\
& \alpha_i                                                         && \text{otherwise}
\end{aligned}
\right.,
\quad
\alpha_i=\frac{1}{d_x^2}-Ze^{x_i}+\frac{\ell(\ell+1)}{2}+\frac{1}{8},
\quad
\beta=-\frac{1}{2d_x^2}.
\end{equation}
The matrices seen in the both sides of Eq.~\eqref{eq:SchrodingerEqMatrixFinite} are both real symmetric.
Especially, the matrix at the right-hand side is a diagonal with positive real numbers, and therefore, is positive definite.
By solving the generalized eigenvalue problem consisting of the two matrices, one can obtain the energies of radial wave functions, $E^{(i)}, (i=1,\cdots,N)$, as the eigenvalues.
One can also obtain the $i$-th radial wave function $R^{(i)}(r)$ as the $i$-th eigenfunction $\xi^{(i)}(x)$, which belongs to the $i$-th eigenenergy $E^{(i)}$.

\section{High-order finite-difference approximation}
The $N_{\mathrm{FD}}$-th order finite-difference formula, which gives the approximation value of the function $f$ at $i$-th grid point, is
\begin{equation}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}f_i
=\frac{1}{d_x^2}\sum_{j=-N_{\mathrm{FD}}}^{+N_{\mathrm{FD}}}c_{j}f_{i+j}
=\frac{1}{d_x^2}(\underbrace{\cdots+c_{-2}f_{i-2}+c_{-1}f_{i-1}}_{\displaystyle N_{\mathrm{FD}}}+c_0f_i+\underbrace{c_{1}f_{i+1}+c_{2}f_{i+2}+\cdots}_{\displaystyle N_{\mathrm{FD}}}),
\label{eq:HighOrderFiniteDifference}
\end{equation}
where $c_{j}$ denotes the finite-difference coefficient to the function value at $(i-j)$-th grid point, $f_{i-j}$.
Substituting the finite-difference formula into the second-order differential equation in the canonical form \eqref{eq:SchrodingerEq6}, one obtains
\begin{equation}
\sum_{j=-N_{\mathrm{FD}}}^{-1}-\frac{1}{2}\frac{c_{j}}{d_x^2}\xi_{i+j}
+\left(-\frac{1}{2}\frac{c_0}{d_x^2}-Ze^{x_i}+\frac{\ell(\ell+1)}{2}+\frac{1}{8}\right)\xi_i
+\sum_{j=1}^{N_{\mathrm{FD}}}-\frac{1}{2}\frac{c_{j}}{d_x^2}\xi_{i+j}
=Ee^{2x_i}\xi_i.
\label{eq:SchrodingerEqDiscrete}
\end{equation}
Note that the first term in the left-hand side is the summation from $j=-N_{\mathrm{FD}}$ to $j=-1$ and the third one the summation from $j=1$ to $j=N_{\mathrm{FD}}$.
Considering this, in the case of high-order finite-difference approximation, the coefficient matrix in the left-hand side of Eq.~\eqref{eq:SchrodingerEqMatrixFinite} is a band matrix with the half width of $N_{\mathrm{FD}}+1$, as follows:
\begin{equation}
\left[
\begin{array}{ccccccccccc}
 \alpha_1'               & \beta_{1}  & \cdots     & \beta_{N_{\mathrm{FD}}} & 0         & \cdots     & \cdots        & \cdots                  & \cdots                  & \cdots        & 0      \\
 \beta_{-1}              & \alpha_2'  & \beta_{1}  &                         & \ddots    & \ddots     &               &                         &                         &               & \vdots \\
 \vdots                  & \beta_{-1} & \alpha_3'  & \beta_{1}               &           & \ddots     & \ddots        &                         &                         &               & \vdots \\
 \beta_{-N_{\mathrm{FD}}} &            & \beta_{-1} & \alpha_4'               & \beta_{1} &            & \ddots        & \ddots                  &                         &               & \vdots \\
 0                       & \ddots     &            & \beta_{-1}              & \alpha_5' & \beta_{1}  &               & \ddots                  & \ddots                  &               & \vdots \\
 \vdots                  & \ddots     & \ddots     &                         & \ddots    & \ddots     & \ddots        &                         & \ddots                  & \ddots        & \vdots \\
 \vdots                  &            & \ddots     & \ddots                  &           & \beta_{-1} & \alpha_{N-4}' & \beta_{1}               &                         & \ddots        & 0      \\
 \vdots                  &            &            & \ddots                  & \ddots    &            & \beta_{-1}    & \alpha_{N-3}'           & \beta_{1}               &               & \beta_{N_{\mathrm{FD}}} \\
 \vdots                  &            &            &                         & \ddots    & \ddots     &               & \beta_{-1}              & \alpha_{N-2}'           & \beta_{1}     & \vdots \\
 \vdots                  &            &            &                         &           & \ddots     & \ddots        &                         & \beta_{-1}              & \alpha_{N-1}' & \beta_{1} \\
 0                       & \cdots     & \cdots     & \cdots                  & \cdots    & \cdots     & 0             & \beta_{-N_{\mathrm{FD}}} & \cdots & \beta_{1}   & \alpha_N'
\end{array}
\right],
\end{equation}
where the non-zero elements of the coefficient matrix are found to be given as
\begin{align}
\alpha_i' &=
\begin{cases}
{\displaystyle \alpha_i+\sum_{j=-N_{\mathrm{FD}}}^{-i}\beta_{j}\exp\left[\left(\ell+\frac{1}{2}\right)jd_x\right] } & \text{for}\;i<N_{\mathrm{FD}} \\
\alpha_i                                                               & \text{otherwise}
\end{cases} \\
\alpha_{i} &= -\frac{1}{2}\frac{c_0}{d_x^2}-Ze^{x_i}+\frac{\ell(\ell+1)}{2}+\frac{1}{8} \\
\beta_{j}  &= -\frac{1}{2}\frac{c_{j}}{d_x^2}.
\end{align}
Since the finite-difference coefficients are $c_{-j}=c_{j}$, the coefficient matrix is real symmetric.

%%%
%%%
%%%
\section{Conjugate-gradient solver for standard eigenproblem}
In the case of uniform radial grids, the discretized Kohn-Sham equation in a matrix form is given as a standard eigenvalue problem, and therefore, the matrix equation can be solved for the radial wave function using an iterative eigenvalue-problem solver based on the conjugate-gradient method for solving linear equation.\cite{PRB42-001394}
The solver does not find all eigenpairs of the Hamiltonian $\mat{H}$, but one can specify the number of eigenpairs to be found as those with the $N_{\text{state}}$ smallest eigenvalues, i.e., $\{ (\varepsilon_{i},\ket{x_{n}}) | n=1,2,\cdots,N_{\text{state}}\}$.
Since the solver is iterative, it is necessary to set appropriate initial vectors to respective eigenvectors $\{\ket{x_{n}} | n=1,2,\cdots,N_{\text{state}}\}$.




In addition, it is also known to set an appropriate preconditioning matrix $\mat{P}$.
As seen in the algorithm below, orthogonalization and normalization operators, ${\cal O}$ and ${\cal N}$, are applied to vectors.
\begin{algorithm}
\caption{Conjugate-gradient solver for a standard eigenvalue problem}\label{alg:CGStandard}
\begin{algorithmic}[1]
\Require $\mat{H}$, $N_{\text{state}}$, and $\{\ket{x_{n}} | n=1,2,\cdots,N_{\text{state}}\}$
\Ensure $\{ (\varepsilon_{i},\ket{x_{n}}) | n=1,2,\cdots,N_{\text{state}}\}$
\For{$n\gets 1,2,\cdots,N_{\text{state}}$} \Comment{loop for states}
\State $\ket{x_{n}}\gets{\cal NO}\ket{x_{n}}$ \Comment{orthonormalize $\ket{x_{n}}$ to $\{\ket{x_{j}}|j=1,2,\cdots,n-1\}$}
\State $i\gets 1$ \Comment{initialize iteration counter}
\Repeat \Comment{loop for conjugate-gradient iteration}
\State $\varepsilon_{n}\gets\braket{x_{n}|\mat{H}|x_{n}}$ \Comment{get eigenvalue}
\State $\ket{r}\gets (\mat{H}-\varepsilon_{n})\ket{x_{n}}$ \Comment{get residual vector}
\State $\ket{s}\gets \mat{P}\ket{r}$ \Comment{precondition $\ket{r}$}
\State $\ket{s}\gets{\cal O}\ket{s}$ \Comment{orthogonalize $\ket{s}$ to $\{\ket{x_{j}}|j=1,2,\cdots,n\}$}
\If{$i=1$}
\State $\ket{p}\gets\ket{s}$ \Comment{initialize conjugate vector}
\Else
\State $\displaystyle \gamma\gets \frac{\braket{s|r}}{\braket{s'|r'}}$
\State $\ket{p}\gets\ket{s}+\gamma\ket{p}$ \Comment{update conjugate vector}
\EndIf
\State $\ket{r'}\gets\ket{r}$
\State $\ket{s'}\gets\ket{s}$
\State $\ket{p}\gets {\cal NO}\ket{p}$ \Comment{orthonormalize $\ket{p}$ only to $\ket{x_{n}}$}
\State $\mat{A}\gets\begin{bmatrix}\braket{x_{n}|\mat{H}|x_{n}} & \braket{x_{n}|\mat{H}|p}\\ \braket{p|\mat{H}|x_{n}} & \braket{p|\mat{H}|p}\end{bmatrix}$ \Comment{get $2\times 2$ Hamiltonian matrix between $\ket{x_{n}}$ \& $\ket{p}$}
\State solve $\mat{A}\begin{bmatrix}\alpha\\ \beta\end{bmatrix}=\lambda\begin{bmatrix}\alpha\\ \beta\end{bmatrix}$ for $\begin{bmatrix}\alpha\\ \beta\end{bmatrix}$ belonging to the \emph{smaller eigenvalue} $\lambda_{\text{small}}$
\State $\ket{x_{n}}\gets\alpha\ket{x_{n}}+\beta\ket{p}$ \Comment{update $\ket{x_{n}}$}
\State $i\gets i+1$
\Until{$\Vert r\Vert < \varepsilon_{\text{tol}}$} \Comment{evaluate convergency}
\EndFor
\end{algorithmic}
\end{algorithm}

%%%
%%%
%%%
\section{Conjugate-gradient solver for generalized eigenproblem}
In the case of logarithmic radial grids, the discretized Kohn-Sham equation in a matrix form is given as a generalized eigenvalue problem.
However, the problem is different from the standard eigenvalue problem which is treated in the previous section, and the main difference is to have the overlap matrix in the right-hand side, meaning that the basis vectors are not orthonomal anymore.
Therefore, the matrix equation can be solved for the radial wave function using an iterative eigenvalue-problem solver based on the conjugate-gradient method for solving linear equation.\cite{ComputPhysCommun134-000033}
The algorithm is below:
\begin{algorithm}
\caption{Conjugate-gradient solver for a generalized eigenvalue problem}\label{alg:CGGeneralized}
\begin{algorithmic}[1]
\Require $\mat{H}$, $\mat{S}$, $N_{\text{state}}$, and $\{\ket{x_{n}} | n=1,2,\cdots,N_{\text{state}}\}$
\Ensure $\{ (\varepsilon_{i},\ket{x_{n}}) | n=1,2,\cdots,N_{\text{state}}\}$
\For{$n\gets 1,2,\cdots,N_{\text{state}}$} \Comment{loop for states}
\State $\ket{x_{n}}\gets{\cal NO}\ket{x_{n}}$ \Comment{orthonormalize $\ket{x_{n}}$ to $\{\ket{x_{j}}|j=1,2,\cdots,n-1\}$}
\State $i\gets 1$ \Comment{initialize iteration counter}
\Repeat \Comment{loop for conjugate-gradient iteration}
\State $\displaystyle \ket{r}\gets\mat{H}\ket{x_{n}}-\mat{S}\sum_{j=1}^{n}\ket{x_{j}}\braket{x_{j}|\mat{H}|x_{n}}$ \Comment{get orthogonalized covariant residual vector}
\State solve $\displaystyle \left(\mat{S}+\frac{\mat{T}}{\tau}\right)\ket{s}=\ket{r}$ for $\ket{s}$ \Comment{get preconditioned contravariant residual vector}
\State $\ket{s}\gets{\cal O}\ket{s}$ \Comment{orthogonalize $\ket{s}$ to $\{\ket{x_{j}}|j=1,2,\cdots,n\}$}
\If{$i=1$}
\State $\ket{p}\gets -\ket{s}$ \Comment{initialize contravariant conjugate vector}
\Else
\State $\displaystyle \gamma\gets \frac{\braket{s|r}-\braket{s|r'}}{\braket{s'|r'}}$
\State $\ket{p}\gets -\ket{s}+\gamma\ket{p}$ \Comment{update contravariant conjugate vector}
\EndIf
\State $\ket{r'}\gets\ket{r}$
\State $\ket{s'}\gets\ket{s}$
\State $\ket{p}\gets{\cal O}\ket{p}$ \Comment{orthogonalize $\ket{p}$ to $\{\ket{x_{j}}|j=1,2,\cdots,n\}$}
\State $\ket{q}\gets{\cal N}\ket{p}$ \Comment{normalize $\ket{p}$}
\State $\mat{A}\gets\begin{bmatrix}\braket{x_{n}|\mat{H}|x_{n}} & \braket{x_{n}|\mat{H}|q}\\ \braket{q|\mat{H}|x_{n}} & \braket{q|\mat{H}|q}\end{bmatrix}$ \Comment{get $2\times 2$ Hamiltonian matrix between $\ket{x_{n}}$ \& $\ket{q}$}
\State $\mat{B}\gets\begin{bmatrix}\braket{x_{n}|\mat{S}|x_{n}} & \braket{x_{n}|\mat{S}|q}\\ \braket{q|\mat{S}|x_{n}} & \braket{q|\mat{s}|q}\end{bmatrix}$ \Comment{get $2\times 2$ overlap matrix between $\ket{x_{n}}$ \& $\ket{q}$}
\State solve $\mat{A}\begin{bmatrix}\alpha\\ \beta\end{bmatrix}=\lambda\mat{B}\begin{bmatrix}\alpha\\ \beta\end{bmatrix}$ for $\begin{bmatrix}\alpha\\ \beta\end{bmatrix}$ belonging to the \emph{smaller eigenvalue} $\lambda_{\text{small}}$
\State $\ket{x_{n}}\gets\alpha\ket{x_{n}}+\beta\ket{q}$ \Comment{update $\ket{x_{n}}$}
\State $\ket{x_{n}}\gets{\cal N}\ket{x_{n}}$ \Comment{normalize $\ket{x_{n}}$}
\State $i\gets i+1$
\Until{$\Vert r\Vert < \varepsilon_{\text{tol}}$} \Comment{evaluate convergency}
\State $\displaystyle \varepsilon_{n}\gets\frac{\braket{x_{n}|\mat{H}|x_{n}}}{\braket{x_{n}|\mat{S}|x_{n}}}$ \Comment{get eigenvalue}
\EndFor
\end{algorithmic}
\end{algorithm}

\appendix

\section{From linear differential equation to canonical form}
Let us think transforming a second-order linear differential equation
\begin{equation}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}y(x)+P(x)\frac{\mathrm{d}}{\mathrm{d}x}y(x)+Q(x)y(x)=R(x)
\label{eq:2ndOrderLinearDifferentialEquation}
\end{equation}
into a canonical form.
In Eq.~\eqref{eq:2ndOrderLinearDifferentialEquation}, an integrand function $y(x)$ is assumed to be denoted by a product of two non-zero functions $u(x)$ and $y_0(x)$ as
\begin{equation}
y(x)=u(x)y_0(x).
\label{eq:CanonicalFormSeparation}
\end{equation}
Then, the first and second derivatives of the function $y(x)$ are given as
\begin{align}
\frac{\mathrm{d}}{\mathrm{d}x}y(x)     &= y_0(x)\frac{\mathrm{d}}{\mathrm{d}x}u(x)+u(x)\frac{\mathrm{d}}{\mathrm{d}x}y_0(x) \\
\frac{\mathrm{d}^2}{\mathrm{d}x^2}y(x) &= y_0(x)\frac{\mathrm{d}^2}{\mathrm{d}x^2}u(x)+2\frac{\mathrm{d}}{\mathrm{d}x}u(x)\cdot\frac{\mathrm{d}}{\mathrm{d}x}y_0(x)+u(x)\frac{\mathrm{d}^2}{\mathrm{d}x^2}y_0(x),
\end{align}
respectively.
Substituting these derivatives into the original second-order linear differential equation \eqref{eq:2ndOrderLinearDifferentialEquation}, it becomes
\begin{multline}
\left(y_0(x)\frac{\mathrm{d}^2}{\mathrm{d}x^2}u(x)+2\frac{\mathrm{d}}{\mathrm{d}x}u(x)\cdot\frac{\mathrm{d}}{\mathrm{d}x}y_0(x)+u(x)\frac{\mathrm{d}^2}{\mathrm{d}x^2}y_0(x)\right)+P(x)\left(y_0(x)\frac{\mathrm{d}}{\mathrm{d}x}u(x)+u(x)\frac{\mathrm{d}}{\mathrm{d}x}y_0(x)\right)\\
+Q(x)u(x)y_0(x)=R(x),
\end{multline}
and it can be ordered with respect to the derivative order of the function $y_0(x)$ as
\begin{multline}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}y_0(x)+\left(\frac{2}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)+P(x)\right)\frac{\mathrm{d}}{\mathrm{d}x}y_0(x)+\left(\frac{1}{u(x)}\frac{\mathrm{d}^2}{\mathrm{d}x^2}u(x)+\frac{P(x)}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)+Q(x)\right)y_0(x)\\
=\frac{R(x)}{u(x)}.
\label{eq:PreCanonicalForm}
\end{multline}

To make this equation a canonical form, it is necessary that the term of the first derivative of the function $y_0(x)$ becomes zero.
The condition is
\begin{equation}
\frac{2}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)+P(x)=0
\quad\Leftrightarrow\quad
\frac{1}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)=-\frac{1}{2}P(x).
\label{eq:CanonicalFormCondition}
\end{equation}
Here, the derivative of the logarithmic function $\ln[f(x)]$ with respect to $x$, where $f(x)$ is arbitrary, is given as
\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}x}\ln[f(x)]=\frac{1}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)
\end{equation}
Using this, the right-hand side of Eq.~\eqref{eq:CanonicalFormCondition} can be transformed into
\begin{align}
\frac{\mathrm{d}}{\mathrm{d}x}\ln[u(x)] &= -\frac{1}{2}P(x) \\
\ln[u(x)] &= -\frac{1}{2}\int\!\!P(x)\mathrm{d}x+C_0 \\
u(x) &= C\exp\left(-\frac{1}{2}\int\!\!P(x)\mathrm{d}x\right), \quad C=e^{C_0},
\end{align}
where $C$ is an arbitrary constant.
Now, we have determined the function $u(x)$, with which the term of the first-derivative of $y_{0}(x)$ in \eqref{eq:PreCanonicalForm} becomes zero.

In the next, let us think to rewrite Eq.~\eqref{eq:PreCanonicalForm} only with $P(x)$ and $Q(x)$.
The derivative of the function
\begin{equation}
\frac{1}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)
\label{eq:func1}
\end{equation}
with respect to $x$ is given as
\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}x}\left(\frac{1}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)\right)=\frac{1}{u(x)}\frac{\mathrm{d}^2}{\mathrm{d}x^2}-\left(\frac{1}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)\right)^2.
\end{equation}
Therefore, one can see
\begin{equation}
\frac{1}{u(x)}\frac{\mathrm{d}^2}{\mathrm{d}x^2}u(x)=\frac{\mathrm{d}}{\mathrm{d}x}\left(\frac{1}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)\right)+\left(\frac{1}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)\right)^2.
\end{equation}
Here, using the right-hand side of Eq.~\eqref{eq:CanonicalFormCondition}, one can express Eq.~\eqref{eq:func1} with $P(x)$ and its derivative with respect to $x$, as
\begin{equation}
\frac{1}{u(x)}\frac{\mathrm{d}^2}{\mathrm{d}x^2}u(x)=-\frac{1}{2}\frac{\mathrm{d}}{\mathrm{d}x}P(x)+\frac{P(x)^2}{4}
\end{equation}
On the other hand, by using the right-hand side of Eq.~\eqref{eq:CanonicalFormCondition}, the term
\begin{equation}
\frac{P(x)}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)
\end{equation}
can be transformed into
\begin{equation}
\frac{P(x)}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)=-\frac{P(x)^2}{2}.
\end{equation}
Therefore, the coefficients to $y_0(x)$ in Eq.~\eqref{eq:PreCanonicalForm} are changed to
\begin{equation}
\frac{1}{u(x)}\frac{\mathrm{d}^2}{\mathrm{d}x^2}u(x)+\frac{P(x)}{u(x)}\frac{\mathrm{d}}{\mathrm{d}x}u(x)+Q(x)=Q(x)-\frac{1}{2}\frac{\mathrm{d}}{\mathrm{d}x}P(x)-\frac{P(x)^2}{4}.
\end{equation}

In conclusion, the canonical form of the second-order linear differential equation \eqref{eq:2ndOrderLinearDifferentialEquation} is
\begin{gather}
\frac{\mathrm{d}^2}{\mathrm{d}x^2}y_0(x)+I(x)y_0(x)=J(x) \\
\text{where}\ I(x)=Q(x)-\frac{1}{2}\frac{\mathrm{d}}{\mathrm{d}x}P(x)-\frac{P(x)^2}{4}
\ \text{and}\
J(x)=\frac{R(x)}{u(x)},
\end{gather}
and from the definition in Eq.~\eqref{eq:CanonicalFormSeparation}, the solution of Eq.~\eqref{eq:2ndOrderLinearDifferentialEquation}, i.e., $y(x)$, is given as
\begin{equation}
y(x)=C\exp\left(-\frac{1}{2}\int\!\!P(x)\mathrm{d}x\right)y_0(x)
\end{equation}

\section{Coefficient matrix for equidistant grids}
In the case that uniform radial grids are used, the Kohn-Sham equation \eqref{eq:SchrodingerEq} is discretized into a matrix equation in the form an a standard eigenvalue problem, and it is numerically solved for the eigenpairs using an eigensolver.
The coefficient matrix is given as
\begin{equation}
\left[
\begin{array}{ccccccccccc}
 \alpha_1               & \beta_{1}  & \cdots     & \beta_{N_{\mathrm{FD}}} & 0         & \cdots     & \cdots        & \cdots                  & \cdots                  & \cdots        & 0      \\
 \beta_{-1}              & \alpha_2  & \beta_{1}  &                         & \ddots    & \ddots     &               &                         &                         &               & \vdots \\
 \vdots                  & \beta_{-1} & \alpha_3  & \beta_{1}               &           & \ddots     & \ddots        &                         &                         &               & \vdots \\
 \beta_{-N_{\mathrm{FD}}} &            & \beta_{-1} & \alpha_4               & \beta_{1} &            & \ddots        & \ddots                  &                         &               & \vdots \\
 0                       & \ddots     &            & \beta_{-1}              & \alpha_5 & \beta_{1}  &               & \ddots                  & \ddots                  &               & \vdots \\
 \vdots                  & \ddots     & \ddots     &                         & \ddots    & \ddots     & \ddots        &                         & \ddots                  & \ddots        & \vdots \\
 \vdots                  &            & \ddots     & \ddots                  &           & \beta_{-1} & \alpha_{N-4} & \beta_{1}               &                         & \ddots        & 0      \\
 \vdots                  &            &            & \ddots                  & \ddots    &            & \beta_{-1}    & \alpha_{N-3}           & \beta_{1}               &               & \beta_{N_{\mathrm{FD}}} \\
 \vdots                  &            &            &                         & \ddots    & \ddots     &               & \beta_{-1}              & \alpha_{N-2}           & \beta_{1}     & \vdots \\
 \vdots                  &            &            &                         &           & \ddots     & \ddots        &                         & \beta_{-1}              & \alpha_{N-1} & \beta_{1} \\
 0                       & \cdots     & \cdots     & \cdots                  & \cdots    & \cdots     & 0             & \beta_{-N_{\mathrm{FD}}} & \cdots & \beta_{1}   & \alpha_N
\end{array}
\right],
\end{equation}
where the non-zero elements of the coefficient matrix are found to be given as
\begin{equation}
\alpha_{i} = -\frac{1}{2}\frac{c_0}{d_x^2}-Ze^{x_i}+\frac{\ell(\ell+1)}{2}
\quad\text{and}\quad
\beta_{j}  = -\frac{1}{2}\frac{c_{j}}{d_x^2}.
\end{equation}
Since the finite-difference coefficients are $c_{-j}=c_{j}$, the coefficient matrix is real band symmetric.


\begin{thebibliography}{9}
\bibitem{ANarita1998} 成田 章, 非経験的原子構造計算の理論と数値解法, 素材物性学雑誌, {\bf 11}(2), 102--130 (1998). \url{doi:10.5188/jsmerj.11.2_102}
\bibitem{PRB42-001394} D. M. Bylander, L. Kleinman, and S. Lee, Phys. Rev. B, {\bf 42}, 1394 (1990). \url{https://doi.org/10.1103/PhysRevB.42.1394}
\bibitem{ComputPhysCommun134-000033} C. K. Gan, P. D. Haynes, and M. C. Payne, Comput. Phys. Commun., {\bf 134}, 33 (2001). \url{https://doi.org/10.1016/S0010-4655(00)00188-0}
\end{thebibliography}

\end{document}