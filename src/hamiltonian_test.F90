
! #define DEBUG

!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!> @file hamiltonian_type_test.F90
!> @brief Fortran module for a class of 1D grids
!> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
!> @copyright &copy; 2023- Shigeru Tsukamoto. All rights reserved.
!--
program hamiltonian_type_test
  use hamiltonian_mod, only : hamiltonian
  use grid_mod, only : grid
  implicit none

  !-- parameters
  character(len=*), parameter :: PROMPT = "hamiltonian_type_test >>>"
  character(len=*), parameter :: TYPE = 'uniform'
  integer,          parameter :: N = 1500
  real(8),          parameter :: XMIN = 1.0d-6
  real(8),          parameter :: XMAX = 60.0d0
  integer,          parameter :: ELL = 0

  !-- local variables
  type(grid) :: g
  real(8) :: v(N)
  type(hamiltonian) :: h

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  g = grid( type=TYPE, n=N, xmin=XMIN, xmax=XMAX )
  v(:) = -1.0d0 / g%x(:) + 0.5d0 * ELL * ( ELL + 1 ) / g%x(:) ** 2
  h = hamiltonian( r=g, v=v )
  call h%show()

  !-- finish test
  stop "STOP: MODULE TEST normally terminated."
end program hamiltonian_type_test
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
