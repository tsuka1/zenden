
#define DEBUG

!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!> @file predictcorrect_smod.F90
!> @brief Fortran submodule implementing subprograms about predictor-corrector method
!> @note reference URL: https://www1.gifu-u.ac.jp/~tanaka/numerical_analysis.pdf
!> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
!> @copyright &copy; 2024- Shigeru Tsukamoto. All rights reserved.
!--
submodule(predictcorrect_mod) predictcorrect_smod
  implicit none
  contains

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- predictcorrect_initialize( scarel, r, z, ell, e, v ) result( pc )
  !--
  module procedure predictcorrect_initialize
    implicit none
    !-- parameters
    character(len=*), parameter :: PROMPT = 'predictcorrect_initialize >>>'
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

    !-- check grid type
    if( trim( r%type ) == 'uniform' )then
      print '(a,x,a)', PROMPT, 'ERROR: code for uniform grid not yet implemented.'
      call abort()
    else if( trim( r%type ) == 'logarithm' )then
      !-- do nothing
    else
      print '(a,2(x,a))', PROMPT, 'ERROR: invalid grit type found. r%type =', trim( r%type )
      call abort()
    end if !-- r%type

    !-- check order
    if( order < ORDERMIN .or. order > ORDERMAX )then
      print '(a,x,a,x,i0)', PROMPT, 'ERROR: invalid order found. order =', order
      call abort()
    end if !-- order

    !-- initialize scalar variables
    this%gridtype = r%type
    this%scarel = scarel
    this%order  = order
    this%z      = z
    this%en     = en
    this%ell    = ell
    this%n      = r%n
    this%h      = r%ds

    !-- initalize array variables
    allocate( this%r(this%n), this%m(this%n), this%v(this%n), this%w(this%n) )
    this%r(:) = r%x(:)
    this%v(:) = v(:)

    !-- set e, mmax, m(:), w(:)
    call this%update_energy( e=e )

    !-- show instance parameters
    call this%show()

    return
  end procedure predictcorrect_initialize
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- predictcorrect_update_energy( e )
  !--
  module procedure predictcorrect_update_energy
    use scalarrelativistic_mod, only : scalarrelativistic_get_m
    implicit none
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    this%e = e
    this%mmax = minloc( abs( this%e - this%v(:) ), dim=1, back=.true. )
    if( this%scarel )then
      this%m(:) = scalarrelativistic_get_m( e=this%e, v=this%v(:) )
      this%w(:) = this%ell * ( this%ell + 1 ) / ( this%r(:) ** 2 * this%m(:) ) &
     &          - 2.0d0 * ( this%e - this%v(:) )
    else
      this%m(:) = 1.0d0
      this%w(:) = this%ell * ( this%ell + 1 ) / this%r(:) ** 2 - 2.0d0 * ( this%e - this%v(:) )
    end if !-- this%scarel
    return
  end procedure predictcorrect_update_energy
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- predictcorrect_gen_m_gl_gr( this, m, gl, gr )
  !--
  module procedure predictcorrect_gen_m_gl_gr
    use scalarrelativistic_mod, only : scalarrelativistic_get_dfdx, scalarrelativistic_get_dgdx, &
      scalarrelativistic_gen_boundary_left, scalarrelativistic_gen_boundary_right
    use adamsbashforth_mod, only : adamsbashforth_get
    use adamsmoulton_mod, only : adamsmoulton_get
    implicit none

    !-- parameters
    real(8), parameter :: GMAX = huge( 1.0e0 )

    !-- local variables
    integer :: iter, i, k
    real(8) :: fl(this%n), fr(this%n), dfdx(this%order), dgdx(this%order)
    real(8) :: fnew, gnew, dfdx0, dgdx0, fold, gold, dgdxm

    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

    !-- initialize
    k = this%order
    fl(:) = 0.0d0
    fr(:) = 0.0d0
    gl(:) = 0.0d0
    gr(:) = 0.0d0

    !-- set boundary conditions (left & right)
    call scalarrelativistic_gen_boundary_left( rel=this%scarel, n=k, z=this%z, ell=this%ell, &
   &                                           e=this%e, r=this%r(1:k), f=fl(1:k), g=gl(1:k) )
    call scalarrelativistic_gen_boundary_right( rel=this%scarel, n=k, z=this%z, ell=this%ell, &
   &  e=this%e, r=this%r(this%n-k+1:this%n), f=fr(this%n-k+1:this%n), g=gr(this%n-k+1:this%n) )

    !-- execute inward shooting procedure (predictor-corrector method)
    do m = this%n - k + 1, 2, -1
      dfdx(:) = -scalarrelativistic_get_dfdx( grid=this%gridtype, rel=this%scarel, r=this%r(m:m+k-1), &
     &                                        w=this%w(m:m+k-1), f=fr(m:m+k-1), g=gr(m:m+k-1) )
      dgdx(:) = -scalarrelativistic_get_dgdx( grid=this%gridtype, rel=this%scarel, r=this%r(m:m+k-1), &
     &                                        m=this%m(m:m+k-1), f=fr(m:m+k-1), g=gr(m:m+k-1) )
      fnew = adamsbashforth_get( order=k, h=this%h, y0=fr(m), f=dfdx(1:k) )
      gnew = adamsbashforth_get( order=k, h=this%h, y0=gr(m), f=dgdx(1:k) )
      do iter = 1, ITERMAX
        dfdx0 = -scalarrelativistic_get_dfdx( grid=this%gridtype, rel=this%scarel, r=this%r(m-1), &
       &                                      w=this%w(m-1), f=fnew, g=gnew )
        dgdx0 = -scalarrelativistic_get_dgdx( grid=this%gridtype, rel=this%scarel, r=this%r(m-1), &
       &                                      m=this%m(m-1), f=fnew, g=gnew )
        fold = fnew
        gold = gnew
        fnew = adamsmoulton_get( order=k, h=this%h, y0=fr(m), fp=dfdx0, f=dfdx(1:k) )
        gnew = adamsmoulton_get( order=k, h=this%h, y0=gr(m), fp=dgdx0, f=dgdx(1:k) )
        if( abs( ( fnew - fold ) / fold ) < TOL .and. abs( ( gnew - gold ) / gold ) < TOL ) exit
      end do !-- iter
      fr(m-1) = fnew
      gr(m-1) = gnew
      if( abs( gnew ) > GMAX )then
        fr(m-1:) = fr(m-1:) * GMAX / gnew
        gr(m-1:) = gr(m-1:) * GMAX / gnew
      end if !-- gnew
      !-- search for inflection point
      dgdxm = scalarrelativistic_get_dgdx( grid=this%gridtype, rel=this%scarel, r=this%r(m-1), &
     &                                     m=this%m(m-1), f=fr(m-1), g=gr(m-1) )
      if( m < this%mmax .and. dgdxm > 0.0d0 )exit
    end do !-- m

    !-- execute outward shooting procedure (predictor-corrector method)
    do i = k, m
      dfdx(:) = scalarrelativistic_get_dfdx( grid=this%gridtype, rel=this%scarel, r=this%r(i-k+1:i), &
     &                                       w=this%w(i-k+1:i), f=fl(i-k+1:i), g=gl(i-k+1:i) )
      dgdx(:) = scalarrelativistic_get_dgdx( grid=this%gridtype, rel=this%scarel, r=this%r(i-k+1:i), &
     &                                       m=this%m(i-k+1:i), f=fl(i-k+1:i), g=gl(i-k+1:i) )
      fnew = adamsbashforth_get( order=k, h=this%h, y0=fl(i), f=dfdx(k:1:-1) )
      gnew = adamsbashforth_get( order=k, h=this%h, y0=gl(i), f=dgdx(k:1:-1) )
      do iter = 1, ITERMAX
        dfdx0 = scalarrelativistic_get_dfdx( grid=this%gridtype, rel=this%scarel, r=this%r(i+1), &
       &                                     w=this%w(i+1), f=fnew, g=gnew )
        dgdx0 = scalarrelativistic_get_dgdx( grid=this%gridtype, rel=this%scarel, r=this%r(i+1), &
       &                                     m=this%m(i+1), f=fnew, g=gnew )
        fold = fnew
        gold = gnew
        fnew = adamsmoulton_get( order=k, h=this%h, y0=fl(i), fp=dfdx0, f=dfdx(k:1:-1) )
        gnew = adamsmoulton_get( order=k, h=this%h, y0=gl(i), fp=dgdx0, f=dgdx(k:1:-1) )
        if( abs( ( fnew - fold ) / fold ) < TOL .and. abs( ( gnew - gold ) / gold ) < TOL ) exit
      end do !-- iter
      fl(i+1) = fnew
      gl(i+1) = gnew
      if( abs( gnew ) > GMAX )then
        fr(:i+1) = fr(:i+1) * GMAX / gnew
        gr(:i+1) = gr(:i+1) * GMAX / gnew
      end if !-- gnew
    end do !-- i

    !-- normalize fl, gl, fr, and gr, and calculate dgdx at m
    fr(:) = fr(:) / gr(m)
    gr(:) = gr(:) / gr(m)
    fl(:) = fl(:) / gl(m)
    gl(:) = gl(:) / gl(m)
    dgldx = scalarrelativistic_get_dgdx( grid=this%gridtype, rel=this%scarel, r=this%r(m), &
   &                                     m=this%m(m), f=fl(m), g=gl(m) )
    dgrdx = scalarrelativistic_get_dgdx( grid=this%gridtype, rel=this%scarel, r=this%r(m), &
   &                                     m=this%m(m), f=fr(m), g=gr(m) )

    return
  end procedure predictcorrect_gen_m_gl_gr
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- predictcorrect_show( this )
  !--
  module procedure predictcorrect_show
    implicit none
    !-- parameters
    character(len=*), parameter :: PROMPT = 'predictcorrect_show >>>'
    character(len=*), parameter :: FMT = '(a,x,a,x,a,2(a,x,i0),a,x,f7.4,3(a,x,i0),a,x,l0)'
    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    write( unit=6, fmt=FMT ) PROMPT, 'grid type:', this%gridtype, ', ngrid =', this%n, ', order =', &
   &  this%order, ', h =', this%h, ', atomic number =', this%z, ', en =', this%en, ', ell =',       &
   &  this%ell, ', sca.rel.:', this%scarel
    return
  end procedure predictcorrect_show
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

end submodule predictcorrect_smod
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
