
! #define DEBUG

!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!> @file quantumnumber_test.F90
!> @brief Fortran module for a class of 1D grids
!> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
!> @copyright &copy; 2023- Shigeru Tsukamoto. All rights reserved.
!--
program quantumnumber_test
  use quantumnumber
  implicit none

  !-- parameters
  character(len=*), parameter :: PROMPT = "quantumnumber_test >>>"
  character(len=2), parameter :: SYMBOLS(3) = [ '1s', '2p', '5f' ]

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

  print *, PROMPT, quantumnumber_get_symbol( en=4, ell=3 )
  print *, PROMPT, quantumnumber_get_en( symbol=SYMBOLS(1) )
  print *, PROMPT, quantumnumber_get_ell( symbol=SYMBOLS(1) )

  !-- finish test
  stop PROMPT // " normally terminated."
end program quantumnumber_test
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
