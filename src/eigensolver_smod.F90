
! #define DEBUG

!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!> @file eigensolver_smod.F90
!> @brief Fortran module implementing subprogram of a switcher function to various eigensolvers
!> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
!> @copyright &copy; 2023- Shigeru Tsukamoto. All rights reserved.
!--
submodule(eigensolver_mod) eigensolver_smod
  implicit none
  contains

  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- eigensolver_solve( mode, r, h, neig, val, vec )
  !--
  !> @brief Fortran function providing a switcher function to various eigensolvers
  !> @author Shigeru Tsukamoto (tsukamoto.shigeru@gmail.com)
  !--
  module procedure eigensolver_solve
    use rmmdiis_mod, only : rmmdiis_solve_standard, rmmdiis_solve_generalized
    use davidson_mod, only : davidson_solve_standard, davidson_solve_generalized
    use cg_mod, only : cg_solve_standard, cg_solve_generalized
    use lapack_solve, only : lapack_solve_standard, lapack_solve_generalized
    use slepc, only : slepc_solve_standard, slepc_solve_generalized
    implicit none

    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

    !-- use RMM-DIIS solver
    if( trim( mode ) == "diis" )then
      if( trim( h%type ) == "standard" )then
        call rmmdiis_solve_standard( g=r, h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else if( trim( h%type ) == "generalized" )then
        call rmmdiis_solve_generalized( g=r, h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else
        print *, "ERROR: invalid Hamiltonian type found. h%type: " // trim( h%type )
        call abort()
      end if !-- h%type

    !-- use Davidson-method solver
    else if( trim( mode ) == "davidson" )then
      if( trim( h%type ) == "standard" )then
        call davidson_solve_standard( g=r, h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else if( trim( h%type ) == "generalized" )then
        call davidson_solve_generalized( g=r, h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else
        print *, "ERROR: invalid Hamiltonian type found. h%type: " // trim( h%type )
        call abort()
      end if !-- h%type

    !-- use conjugate-gradient solver
    else if( trim( mode ) == "cg" )then
      if( trim( h%type ) == "standard" )then
        call cg_solve_standard( g=r, h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else if( trim( h%type ) == "generalized" )then
        call cg_solve_generalized( g=r, h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else
        print *, "ERROR: invalid Hamiltonian type found. h%type: " // trim( h%type )
        call abort()
      end if !-- h%type

    !-- use LAPACK solver
    else if( trim( mode ) == "lapack" )then
      if( trim( h%type ) == "standard" )then
        call lapack_solve_standard( h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else if( trim( h%type ) == "generalized" )then
        call lapack_solve_generalized( h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else
        print *, "ERROR: invalid Hamiltonian type found. h%type: " // trim( h%type )
        call abort()
      end if !-- h%type

    !-- use SLEPc solver
    else if( trim( mode ) == "slepc" )then
      if( trim( h%type ) == "standard" )then
        call slepc_solve_standard( h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else if( trim( h%type ) == "generalized" )then
        call slepc_solve_generalized( h=h, neig=neig, val=val(:), vec=vec(:,:) )
      else
        print *, "ERROR: invalid Hamiltonian type found. h%type: " // trim( h%type )
        call abort()
      end if !-- h%type

    !-- error
    else
      print '(a,x,a)', "ERROR: invalid mode found. mode =", mode
      call abort()
    end if !-- mode

    return
  end procedure eigensolver_solve
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

end submodule eigensolver_smod
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
